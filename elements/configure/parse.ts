const _ = require("lodash");

function substitute(file, p) {
    let success = false
    let replaced = p.replace(/\${([\w.-]+)}/g, function (match, term) {
        if (!success) {
            success = _.has(file, term)
        }
        return _.get(file, term) || match
    })
    return {success: success, replace: replaced}
}

function transform(file, obj) {
    let changed = false
    let resultant = _.mapValues(obj, function (p) {
        if (_.isPlainObject(p)) {
            let transformed = transform(file, p)
            if (!changed && transformed.changed) {
                changed = true
            }
            return transformed.result
        }
        if (_.isString(p)) {
            let subbed = substitute(file, p)
            if (!changed && subbed.success) {
                changed = true
            }
            return subbed.replace
        }
        if (_.isArray(p)) {
            for (let i = 0; i < p.length; i++) {
                if (_.isString(p[i])) {
                    p[i] = substitute(file, p[i]).replace
                }
            }
        }
        return p
    })
    return {changed: changed, result: resultant}
}

function readAndSwap<T>(obj: T): T {


    let altered = false
    do {
        let temp = transform(obj, obj)
        obj = temp.result
        altered = temp.changed
    } while (altered)
    return obj
}

export default readAndSwap;
