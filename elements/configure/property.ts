import {filter, map} from "rxjs/operators";
import {Subject} from 'rxjs'
import {Observable} from 'object-observer/dist/node/object-observer.js'

function getPath(obj: Object, path: string): { error: boolean, path: string, value: any } {
    const pathArr = path.split('.');
    let parts = [];
    while (pathArr.length !== 0) {
        let name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            obj = obj[name];
        } else {
            return {error: true, path: parts.join('.'), value: obj};
        }
    }
    return {error: false, path: parts.join('.'), value: obj};
}
function setPath(obj: Object, path: string, value: any) {
    const pathArr = path.split('.');
    let parts = [];
    while (pathArr.length !== 0) {
        let name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            if (pathArr.length === 0) {
                obj[name] = value;
            }else{
                obj = obj[name];
            }

        } else {
            if (pathArr.length === 0) {
                obj[name] = value;
            } else {
                obj[name] = {};
                obj = obj[name];
            }
        }
    }
}
function removePath(obj: Object, path: string) {
    const pathArr = path.split('.');
    let parts = [];
    while (pathArr.length > 0) {
        let name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            if (pathArr.length === 0) {
                delete obj[name];
                return;
            }else{
                obj = obj[name];
            }

        } else {
            if (pathArr.length === 0) {
                delete obj[name];
            } else {
                return;
            }
        }
    }
}
function selectCheck(src: string[], dst: string[]): boolean {
    for (let i of src.keys()) {
        if (src[i] !== dst[i]) {
            return (i > 0) ? true : false;
        }
    }
    return true;
}
function selectSlice(src: string[], dst: string[]): string[] {
    for (let i of src.keys()) {
        if (src[i] !== dst[i]) {
            if (i > 0) {
                return dst.slice(i - 1);
            }
            throw new Error(`ох , бл...`);
        }
    }
    return dst.slice(src.length - 1);
}

export interface Change<T> {
    type: string | 'update' | 'insert' | 'delete' | 'reverse' | 'shuffle',
    oldValue?: any,
    value?: any,
    path: string[],
    object:T
    link: string
}

const logger = require('debug')('element:config:property');
const log = {
    error: logger.extend('error'),
    info: logger.extend('info'),
    log: logger.extend('log'),
}
export class ConfigProperty<T = any> {
    readonly subject: Subject<Change<T>> = new Subject();
    default: T;
    protected store: Map<string, ConfigProperty> = new Map();

    constructor(protected parent?: { store: ConfigProperty; path: string }) {

    }

    protected _data: T;

    get data(): T {
        if(this.parent && !this._data){
            const {store, path} = this.parent;
            this._data = store.get(path, this.default);
        }
        return this._data;

    }

    set data(value: T) {
        if (this.parent) {
            const {store, path} = this.parent;
            store.set(path, value);
            const pathForm = path.split('.');
            store.subject
                .pipe(
                    filter(value1 => selectCheck(pathForm, value1.path)),
                    map(value1 => {
                        value1.path = selectSlice(value1.path, pathForm);
                        value1.link = value1.path.join('.');
                        return value1;
                    })
                ).subscribe(this.subject)
            this._data = value;
        } else {
            const data  = Observable.from(value);
            data.observe(changes => {
                changes.forEach(change => {
                    this.subject.next({
                        ...change,
                        link: change.path.join('.')
                    })
                })
            });
            this._data = data;
        }


    }

    get absolute(): string {
        return (this.parent) ? this.parent.path : ''
    }

    get(path?: string, value?: any) {
        if(path === undefined || path === '' || !path|| path === null){
            return this.data;
        }
        const result = getPath(this.data, path);
        if (result.error) {
            if (value !== undefined) {
                return value;
            }
            const error = new Error(`action: get path: ${path} full: ${this.full(path)} found: ${result.path}`);
            log.error(error.message);
            throw error;
        }
        log.log(`action: get path: ${path} full: ${this.full(path)}`, JSON.stringify(result.value))
        return result.value;
    }

    set(path: string, value: any): this {
        setPath(this.data, path, value);
        log.log(`action: set path: ${path} full: ${this.full(path)}`, JSON.stringify(value))
        return this;
    }

    has(path: string): boolean {
        return !getPath(this.data, path).error;
    }

    delete(path: string): this {
        removePath(this.data, path);
        log.log(`action: delete path: ${path} full: ${this.full(path)}`)
        return this;
    }
    full(name?:string):string{
        let result = [];
        if(name){
            result.push(name);
        }
        if(this.parent) {
            result.push(this.parent.path);
            result.push(this.parent.store.full());
        }
        return result
            .filter(value => value !== '')
            .reverse()
            .join('.');
    }
    use<C = T>(path:string):ConfigProperty<C>{
        if(!this.has(path)){
            throw new Error(`нет такого ${path}`)
        }
        if (this.store.has(path)) {
            return this.store.get(path)
        }
        const store = new ConfigProperty({
            store: this,
            path: path,
        });
        this.store.set(path, store);
        log.log(`action: use path: ${path} full: ${this.full(path)}`)
        return store;
    }
}
