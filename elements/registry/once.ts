import {isAbsolute, join, normalize, relative} from "path";

export interface ConfigOnce {
    root: string,
    store: { [path: string]: string }
}

export class Once {
    readonly names: Set<string> = new Set();
    protected store: Map<string, string> = new Map();
    public root: string;

    constructor(value?: ConfigOnce) {
        if (value) {
            const {store, root} = value;
            this.root = root;
            this.load(store);
        }
    }

    absolute(path: string) {
        return (isAbsolute(path)) ? normalize(path) : join(this.root, path);
    }

    relative(path: string) {
        return (isAbsolute(path)) ? relative(this.root, path) : path;

    }

    [Symbol.iterator](): IterableIterator<[string, string]> {
        return this.store[Symbol.iterator]();
    }

    entries(): IterableIterator<[string, string]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<string> {
        return this.store.values();
    }

    set(path: string, name: string,): this {
        const link = this.relative(path);
        if (this.store.has(link)) {
            this.names.delete(this.store.get(link));
        }
        this.store.set(link, name);
        this.names.add(name);
        return this;
    }

    add(path: string, name: string): this {
        const link = this.relative(path);
        if (this.store.has(link) && this.names.has(name)) {
            return this;
            // throw new Error(`duplicate name:${name} path:${path}`);
        }
        this.store.set(link, name);
        this.names.add(name);
        return this;
    }

    remove(value: string) {
        if (this.store.has(value)) {
            this.names.delete(this.store.get(value));
            this.store.delete(value);
            return;
        }
        const path = this.relative(value);
        for (let [name, link] of this.store.entries()) {
            if (path == link) {
                this.names.delete(name);
                this.store.delete(link);
                return;
            }
        }
    }


    path(name: string): string | false {
        if (this.store.has(name)) {
            return this.absolute(this.store.get(name));
        }
        return false;
    }


    name(path: string): string | false {
        const link = this.relative(path);
        for (let [name, link1] of this.store.entries()) {
            if (link1 == link) {
                return name;
            }
        }
        return false
    }

    load(data: { [name: string]: string }): this {
        Object.keys(data).forEach(value => {
            this.add(data[value],value);
        });
        return this;
    }


    save(absolute: boolean = false): { [path: string]: string } {
        let result = {};
        this.store.forEach((value, key) => {
            result[value] =  (absolute) ? this.absolute(key) : key;
        });
        return result;
    }

    toObject(): {
        root: string
        store: { [path: string]: string }
    } {
        return {
            root: this.root,
            store: this.save()
        }
    }

    toJson() {
        return JSON.stringify(this.toObject());
    }

    has(path: string): boolean {
        return this.store.has(path);
    }

    clear() {
        this.store.clear();
        this.names.clear();
    }

    paths(absolute: boolean = true): string[] {
        return Array.from(this.store.keys())
            .map(value => (absolute) ? this.absolute(value) : value);
    }
}
