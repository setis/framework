import {Base} from "./base";
import {applyMiddleware, combineReducers, compose, createStore, Reducer, Store} from "redux";
import {combineEpics} from "redux-observable";
import {Action} from "./action";

export class Root extends Base{

    protected _action: Action = new Action(this);

    get actions(): { [name: string]: any } {
        return this._action.actions;
    }

    set actions(value: { [name: string]: any }) {
        this._action.actions = value;
    }

    get reducer(): Reducer {
        let result = {};
        for (let store of this.childrens) {
            result[store.name] = store.reducer;
        }
        return combineReducers(result);
    }

    get epic() {
        let result = [];
        for (let store of this.childrens) {
            result.push(store.epic);
        }
        return combineEpics(result)
    }

    protected _store: Store;

    get store() {
        if (!this._store) {
            const epic = this.middleware("epic");
            this._store = createStore(
                this.reducer,
                compose(
                    applyMiddleware(this.middleware('thunk')),
                    applyMiddleware(epic)
                )
            );
            // @ts-ignore
            epic.run(this.epic)
        }
        return this._store;
    }

}
