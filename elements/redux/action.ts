import {bindActionCreators} from "redux";
import {Base} from "./base";

export class Action<A = any> {
    protected store: Map<string, A> = new Map();

    constructor(protected container: Base) {
    }

    private _actions: { [name: string]: any };

    get actions(): { [p: string]: any } {
        return this._actions;
    }

    set actions(value: { [p: string]: any }) {
        this._actions = value;
        this.run();
    }


    [Symbol.iterator](): IterableIterator<[string, A]> {
        return this.store[Symbol.iterator]();
    }

    entries(): IterableIterator<[string, A]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<A> {
        return this.store.values();
    }

    run() {
        this.store.clear();
        for (let store of this.container.childrens) {
            const {actions, name} = store;
            for (let key in actions) {
                this.store.set(`${name}.${key}`, actions[key])
            }
        }
        this.load(this._actions);

    }

    load(value: Map<string, any> | { [action: string]: any }) {
        if (value instanceof Map) {
            value.forEach((value1, key) => this.store.set(key, bindActionCreators(value1, this.container.dispatch)))
        } else if (value instanceof Object) {
            Object.keys(value)
                .forEach(key => this.store.set(key, bindActionCreators(value[key], this.container.dispatch)))
        } else {
            throw new Error(`ох , бл...`);
        }
    }


    use(name: string) {
        if (!this.store.has(name)) {
            throw new Error(`ох , бл...`);
        }
        return this.store.get(name);
    }

}
