import {applyMiddleware, combineReducers, compose, createStore, Middleware, Reducer, Store} from "redux";
import {combineEpics, createEpicMiddleware} from "redux-observable";
import {Action} from "./action";

export class Base {
    name: string;
    middlewares: Map<string, Middleware> = new Map();
    childrens: Set<Base> = new Set();

    protected _action: Action = new Action(this);

    get actions(): { [name: string]: any } {
        return this._action.actions;
    }

    set actions(value: { [name: string]: any }) {
        this._action.actions = value;
    }

    protected _store: Store;

    get store() {
        if (!this._store) {
            const epic = this.middleware("epic");
            this._store = createStore(
                this.reducer,
                compose(
                    applyMiddleware(this.middleware('thunk')),
                    applyMiddleware(epic)
                )
            );
            // @ts-ignore
            epic.run(this.epic)
        }
        return this._store;
    }

    get dispatch() {
        return this.store.dispatch
    }

    get reducer(): Reducer {
        let result = {};
        for (let store of this.childrens) {
            result[store.name] = store.reducer;
        }
        return combineReducers(result);
    }

    get epic() {
        let result = [];
        for (let store of this.childrens) {
            result.push(store.epic);
        }
        return combineEpics(result)
    }


    middleware(name: string) {
        if (this.middlewares.has(name)) {
            return this.middlewares.get(name);
        }
        let middleware: Middleware;
        switch (name) {
            case "epic":
                middleware = createEpicMiddleware();
                break;
            case "thunk":
                middleware = require('redux-thunk');
                break;
            default:
                throw new Error(`ох , бл...`);
        }
        this.middlewares.set(name, middleware);
        return middleware;
    }

    state() {
        return this.store.getState()
    }

    action<T = any>(name: string): T {
        if (!this._action) {
            this._action = new Action(this);
        }
        return this._action.use(name)
    }

}
