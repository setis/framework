import {Dispatch, Reducer, Store} from "redux";

export interface Api<S> {

    reducer: Reducer;

    actions: { [action: string]: any };
    epic: any;
    store: Store<S>
    state: S
    dispatch: Dispatch

    action<T>(name: string): T;
}
