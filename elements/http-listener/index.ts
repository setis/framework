import {nosafe, safe} from './http@1';
import {nosafe as nosafe2, safe as safe2} from './http@2';
import {SecureServerOptions} from "http2";
import {ServerOptions} from "https";
import {Configure, Handler, Listen} from "./interface";

export * from './interface';
export default function Create(version: string | number, listen: Listen, cfg: SecureServerOptions | ServerOptions | false = false, request?: Handler) {
    let p;
    switch (version) {
        case '2':
        case '2.0':
        case 2:
            p = (cfg) ? safe2(cfg, listen, request) : nosafe2(listen, request);
            break;
        case '1':
        case '1.1':
        case 1:
        default:
            p = (cfg) ? safe(cfg, listen, request) : nosafe(listen, request);
            break;

    }
    return p;
}
export const defaults: Configure = {
    protocol: 1,
    safe: false,
    port: 80,
    hostname: '0.0.0.0'
};

export function build(config: Configure): string {
    let {port, hostname} = {...defaults, ...config};
    return `${hostname}:${port}`;
}

export function parse(data: string): { hostname: string, port: number } {
    let [hostname, port] = data.split(':');
    return {hostname, port: Number(port)}
}

export function config(data: any): Configure {
    switch (typeof data) {
        case "object":
            return {...defaults, ...data};
        case "string":
            return {...defaults, ...parse(data)}
        default:
            throw new Error(`ох , бл...`);
    }
}

export function check(data: Object): boolean {
    return ["hostname", "port", "protocol", "safe"]
        .filter(value => data.hasOwnProperty(value))
        .length > 0
}
