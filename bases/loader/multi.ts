import {promisify} from "util";
import {readdir} from "fs";
import {join} from "path";
import {PathComponent} from "../../elements/path";
import {RegistryMulti} from "../../elements/registry";
import {ConfigProperty} from "../../elements/configure";

export interface LoaderMultiConfigure {
    path?: string[];
    package?: string[];
    registry?: {
        [path: string]: string[];
    };
    mode:string[];
}
const logger = require('debug')('bases:loader:multi');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
export abstract class LoaderMulti {
    path: PathComponent;
    registry: RegistryMulti;
    notification: (...args) => void;
    config: ConfigProperty<LoaderMultiConfigure>;
    package: string[] = [];
    readonly loading:Set<string> = new Set();
    async run(): Promise<void> {
        for (let name of this.config.get('loader.mode',[])) {
            await this[`loader_${name}`]();
        }
    }

    async loader_path() {
        await Promise.all(
            this.path.save(true).map(value => this.dir(value))
        );
    }


    async loader_registry() {
        await Promise.all(
            this.registry.paths(true)
                .filter(value => !this.loading.has(value))
                .map(this.file.bind(this))
        )
    }

    async loader_package() {
        if(this.package){
            await Promise.all(
                this.package
                    .filter(value => !this.loading.has(value))
                    .map(this.file.bind(this))
            )
        }
    }
    async dir(dir: string): Promise<string[]> {
        let list:string[];
        try{
            list = await promisify(readdir)(dir);
        }catch (e) {
            log.error(`class ${this.constructor.name} action: dir path: ${this.path.relative(dir)} link: ${dir}`,e);
            this.notification('error','dir',e);
            return [];
        }

        const container = await Promise.all(
            list
                .map(file => join(dir, file))
                .filter(value => !this.loading.has(value))
                .map(file => this.file(file))
        );
        const names = []
            .concat(...container)
            .filter(value => value !== undefined);
        log.info(`class ${this.constructor.name} action: dir names: ${names.join(',')} path: ${this.path.relative(dir)} link: ${dir}`)
        this.notification(
            "info",
            `class ${this.constructor.name} action: dir names: ${names.join(',')} path: ${this.path.relative(dir)} link: ${dir}`
        );
        return names;
    }

    async file(path: string): Promise<string[]|void> {
        let names: string[] = [];
        try {
            const module = require(path);
            names = await this.handler(module, path);
            this.loading.add(path);
        } catch (e) {
            if(/Cannot find module.*?/gm.test(e.message)){
                this.notification(
                    "warn",
                    `class ${this.constructor.name} action: file msg:проверти существования папки path: ${this.path.relative(path)}`
                );
                log.warn(`class ${this.constructor.name} action: file msg: проверти существования папки path: ${this.path.relative(path)}`)
            }else{
                log.error(`class ${this.constructor.name} action: file name: ${e.name} msg: ${e.message} path: ${this.path.relative(path)}`,e.stack);
                this.notification(
                    "error",
                    `class ${this.constructor.name} action: file name: ${e.name} msg: ${e.message} path: ${this.path.relative(path)}`
                );
            }
            return;
        }
        this.notification(
            "info",
            "file",
            `names: ${names.join(', ')} path: ${this.path.relative(path)} link: ${path}`
        );
        log.info(`class ${this.constructor.name} action: file names: ${names.join(', ')} path: ${this.path.relative(path)} link: ${path}`)
        return names;
    }


    abstract async handler(data: any, path: string): Promise<string[]>;

}

