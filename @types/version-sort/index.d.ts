export default function (versions: string[], options?: {
    ignore_stages: boolean,
    nested: boolean
}): string[]
