
declare module 'object-observer' {
    export type Data<T> = DataBase<T> | DataInsert<T> | DataRemove<T> | DataUpdate<T>;

    interface DataBase<T> {
        type: string | 'update' | 'insert' | 'delete' | 'reverse' | 'shuffle',
        oldValue?: any,
        value?: any,
        path: string[],
        object:T
    }

    interface DataInsert<T> extends DataBase<T>{
        type:'insert',
        path:string[],
        value: any,
        object:T
    }

    interface DataUpdate<T> extends DataBase<T>{
        type: 'update',
        path: string[],
        value: any,
        oldValue:any,
        object:T
    }

    interface DataRemove<T> extends DataBase<T>{
        type: 'remove',
        path: string[],
        oldValue:any,
        object:T
    }

    export interface ObserverBase<T> {
        observe(value: (changes: Data<T>[]) => void):void;

        unobserve():void;

        revoke():void;
    }

    export namespace Observable {
        export function from<T>(value: T): T & ObserverBase<T>;

        export function isObservable(target: any): boolean;
    }
}
