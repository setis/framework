import {Component, ComponentComponent} from "../system";
import {Subject, Subscription} from "rxjs";
import Application = require("koa");

export const name = 'http';
export const dependencies = [
    'component://middleware',
    'component://controller'
];
const logger = require('debug')('components:http');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
export default class Http implements Component{
    app: Application;
    readonly subject: Subject<boolean> = new Subject();
    readonly subscription: Subscription = new Subscription();
    private _state: boolean = false;

    constructor(readonly api:ComponentComponent){

    }
    get state(): boolean {
        return this._state;
    }

    set state(value: boolean) {
        this._state = value;
        this.subject.next(value);
    }

    protected _controller: any;

    get controller() {
        if (!this._controller) {
            throw new Error(`ох , бл...`)
        }
        return this._controller;
    }

    get config() {
        return this.api.config;
    }

    async start() {
        if (!this.state) {
            this.state = true;
        }
    }

    async stop() {
        if (this.state) {
            this.state = false;
        }
    }

    async onInit() {
        if (this.subscription) {
            this.subscription
                .add(
                    this
                        .api
                        .app
                        .updown
                        .up('component://controller')
                        .subscribe(value => {
                            log.debug(`${this.constructor.name}:  up ${value.api.constructor.name}`)
                            this._controller = value.api;
                            this.subscription.add(this._controller.subscription);
                        })
                )
                .add(
                    this
                        .api
                        .app
                        .updown
                        .down('component://controller')
                        .subscribe(value => {
                            log.debug(`${this.constructor.name}: down ${this._controller.name}`)
                            this.subscription.remove(this._controller.subscription);
                            delete this._controller;
                        })
                )
            ;
            this.api.app.subscription.add(this.subscription);
        }
    }

    async onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.api.app.subscription.remove(this.subscription);
            if (this._controller) {
                this.subscription.remove(this._controller.subscription);
            }

        }
    }

    // async route(data?: { config?: Actions.Route, app?: Application, router?: Router }) {
    //     let {app = this.app, router = new Router(), config = this.config.get('route', [])} = data;
    //     for (const uri in config) {
    //         const routes = config[uri];
    //         for (const method in routes) {
    //             /**
    //              * заменяем uri swagger -> express
    //              * @example /new/{id} -> /new/:id
    //              * @type {string}
    //              */
    //             const uriNew = uri.replace(/(\{(.*?)\})/g, (str, p1, p2, offset, s) => {
    //                 return `:${p2}`;
    //             });
    //             let actions = [];
    //
    //             for (const route of routes[method]) {
    //                 for (const action in route) {
    //                     const routes = await this.action(action, route[action]);
    //                     routes.forEach(value => actions.push(value));
    //                 }
    //             }
    //             router[(method === '*') ? 'all' : method](uriNew, ...actions);
    //         }
    //     }
    //     app
    //         .use(router.routes())
    //         .use(router.allowedMethods())
    // }
    //
    // async action(action: Actions.Type, configure: Actions.Config): Promise<Middleware[]> {
    //     let routes = [];
    //
    //     switch (action) {
    //         case "middleware":
    //             switch (typeof configure) {
    //                 case "string":
    //                     const route = await this.middleware.use(configure);
    //                     routes.push(route);
    //                     break;
    //                 case "object":
    //                     for (const name in configure) {
    //                         const route = await this.action(action, configure);
    //                         routes.push(...route);
    //                     }
    //                     break;
    //                 default:
    //                     throw new Error(`ох , бл...`);
    //             }
    //             break;
    //         case "controller":
    //             switch (typeof configure) {
    //                 case "string":
    //                     const route = this.controller.use(configure);
    //                     routes.push(route);
    //                     break;
    //                 case "object":
    //                     for (const name of configure as string[]) {
    //                         routes.push(this.controller.use(name));
    //                     }
    //                     break;
    //                 default:
    //                     throw new Error(`ох , бл...`);
    //             }
    //             break;
    //         case "gateway":
    //             let {driver, channel, config} = configure as GateWays.Config;
    //             routes.push(this.gateway.use(driver).gateway(channel, config));
    //             break;
    //         default:
    //             throw new Error(`ох , бл...`);
    //     }
    //     return routes;
    // }
}
