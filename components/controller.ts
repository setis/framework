import {LoaderMulti} from "../bases/loader";
import {Component, ComponentComponent} from "../system";
import {Middleware} from "koa";
import {Subject, Subscription} from "rxjs";

export interface ControllerListProperty {
    [controller: string]: Middleware

}

export type ControllerList = Map<string, Middleware> | ControllerListProperty;
export type ControllerHandler = ((container: any) => Promise<ControllerList> | ControllerList) | ControllerList;

export interface ControllerPackage {
    default: ControllerHandler
}

export const name = 'controller';
export const path = module.id;
export const dependencies = [
    'component://http'
];
const logger = require('debug')('components:controller');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
/**
 * @todo добавить ленивую загрузку (lazyload)
 */
export default class Controller extends LoaderMulti implements Component {
    readonly store: Map<string, Middleware> = new Map();
    readonly loading: Set<string> = new Set();
    readonly loaded: Subject<[string, boolean]> = new Subject();
    readonly subject: Subject<boolean> = new Subject();
    readonly subscription: Subscription = new Subscription();

    constructor(readonly api: ComponentComponent) {
        super();
    }

    private _state: boolean = false;

    get state(): boolean {
        return this._state;
    }

    set state(value: boolean) {
        this._state = value;
        this.subject.next(value);
    }

    protected _container: any;

    get container() {
        if (!this._container) {
            throw new Error(`ох , бл...`)
        }
        return this._container;
    }

    register(name: string, middleware: Middleware) {
        if (!this.store.has(name)) {
            throw new Error(`уже есть такой контроллер: ${name}`);
        }
        this.store.set(name, middleware);
        this.loaded.next([name, true]);
    }

    unregister(name: string) {
        if (this.store.has(name)) {
            this.store.delete(name);
            this.loaded.next([name, false]);
        }
    }

    use(controller: string) {
        if (!this.store.has(controller)) {
            throw new Error(`не найден сконтроллер: ${controller}`);
        }
        return this.store.get(controller);
    }

    async handler(data: ControllerPackage, path: string): Promise<string[]> {
        const container = data.default;
        const list: Set<string> = new Set();
        let controllers: ControllerList;
        switch (typeof container) {
            case "object":
                controllers = container;
                break;
            case "function":
                controllers = await container(this.container);
                break;
            default:
                throw new Error(`ох , бл...`);
        }
        if (controllers instanceof Map) {
            for (let [controller, middleware] of controllers) {
                this.register(controller, middleware);
                list.add(controller)
            }
            this.registry.add(path, Array.from(controllers.keys()))
        } else if (controllers instanceof Object) {
            for (let controller in controllers) {
                let middleware = controllers[controller];
                this.register(controller, middleware);
                list.add(controller)
            }
            this.registry.add(path, Object.keys(container))
        } else {
            throw new Error(`ох , бл...`)
        }
        const controllerList = Array.from(list.values());
        this.registry.add(path, controllerList);
        return controllerList;
    }

    async onInit() {
        if (this.subscription) {
            this.subscription
                .add(
                    this
                        .api
                        .app
                        .updown
                        .up('component://http')
                        .subscribe(value => {
                            log.debug(`${this.constructor.name}:  up ${value.api.constructor.name}`)
                            this._container = value.api;
                            this._container.subscription.add(this.subscription);
                        })
                )
                .add(
                    this
                        .api
                        .app
                        .updown
                        .down('component://http')
                        .subscribe(value => {
                            log.debug(`${this.constructor.name}: down ${this._container.constructor.name}`)
                            delete this._container;
                        })
                )
            ;
            this.api.app.subscription.add(this.subscription);
        }
    }

    async onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.api.app.subscription.remove(this.subscription);
            if (this._container) {
                this._container.subscription.remove(this.subscription);
            }

        }
    }

    async start() {
        if (!this.state) {
            await this.run();
            this.state = true;
        }
    }

    async stop() {
        if (this.state) {
            this.loading.clear();
            this.registry.clear();
            for (const id of this.store.keys()) {
                this.unregister(id);
            }
            this.state = false;
        }
    }

}
