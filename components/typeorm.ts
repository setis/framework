import {join, parse} from "path";
import {
    ConnectionManager,
    ConnectionOptions,
    EntitySchema,
    getConnectionManager,
    getCustomRepository,
    getRepository,
    getTreeRepository,
    ObjectType,
    Repository,
    TreeRepository
} from "typeorm";
import {PathBase} from "../elements/path";
import {Component, ComponentComponent} from "../system";
import {Subject} from "rxjs";

export const name = 'typeorm';
export const path = module.id;
export const version = require(join(parse(require.resolve('typeorm')).dir, 'package.json')).version;

export interface OrmConfigure {
    [name: string]: ConnectionOptions;
}



export default class TypeOrm implements Component {
    protected tree: Map<string, TreeRepository<any>> = new Map();
    protected custom: Map<string, any> = new Map();
    protected entity: Map<string, Repository<any>> = new Map();
    protected manager: ConnectionManager;
    protected path: PathBase;
    readonly subject: Subject<[string, boolean]> = new Subject();
    constructor(readonly api: ComponentComponent) {
    }
    get app(){
        return this.api.app;
    }
    get config(){
        return this.api.config;
    }
    async onInit(): Promise<void> {
        if (!this.path) {
            this.path = new PathBase(this.app.path)
        }
        if(!this.manager){
            this.manager = getConnectionManager()
        }
    }

    async start() {
        const config = Object.keys(this.config.get())
            .map(name => {
                let cfg: ConnectionOptions = {...this.config.get<ConnectionOptions>(name)};
                ["entities", "migrations", "subscribers", 'entitySchemas']
                    .forEach(value => {
                        cfg[value] = [
                            ...this
                                .config
                                .get<string[]>(`${name}.${value}`)
                                .filter(value => value !== undefined)
                                .map(value => this.path.absolute(value)
                                )];
                    });
                if (cfg.cli) {
                    Object.keys(cfg.cli).forEach(value => cfg.cli[value] = this.path.absolute(cfg.cli[value]));
                }
                return {name: name, ...cfg};
            });
        for (const cfg of config) {
            const connection = (!this.manager.has(cfg.name)) ? this.manager.create(cfg) : this.manager.get(cfg.name);
            if (!connection.isConnected) {
                try {
                    await connection.connect();
                    this.subject.next([cfg.name, true])
                } catch (e) {
                    console.log(cfg.name,e.message);
                    this.subject.error(e);
                }

            }

        }

    }

    status():Array<[string,boolean]>{
        if(!this.manager){
            this.manager = getConnectionManager()
        }
        return Object
            .keys(this.config.get())
            .map(value => {
                if(!this.manager.has(value)){
                    return [value,false]
                }
                return [value,this.manager.get(value).isConnected];
            });
    }

    async stop() {
        await Promise.all(
            Object.keys(this.config.get())
                .filter(value => this.manager.has(value))
                .map(value => this.manager.get(value))
                .filter(value => value.isConnected)
                .map(async value => {
                    await value.close();
                    this.subject.next([value.name, false]);
                })
        );
    }


    async onDestroy(): Promise<void> {
        await this.stop();
    }


    repositoryTree<Entity>(entityClass: ObjectType<Entity> | string, connectionName: string = 'default'): TreeRepository<Entity> {
        const entity = (typeof entityClass === 'string') ? entityClass : entityClass.name;
        const id = `${connectionName}:${entity}`;
        if (this.tree.has(id)) {
            // @ts-ignore
            return this.tree.get(id);
        }
        const container = getTreeRepository<Entity>(entityClass, connectionName);
        this.tree.set(id, container);
        return container;
    }

    repositoryCustom<T>(customRepository: ObjectType<T>, connectionName: string = 'default'): T {
        const entity = (typeof customRepository === 'string') ? customRepository : customRepository.name;
        const id = `${connectionName}:${entity}`;
        if (this.custom.has(id)) {
            return this.custom.get(id);
        }
        const container = getCustomRepository(customRepository, connectionName);
        this.custom.set(id, container);
        return container;
    }

    repositoryEntity<Entity>(entityClass: ObjectType<Entity> | EntitySchema<Entity> | string, connectionName: string = 'default'): Repository<Entity> {
        const entity: string = (typeof entityClass === 'string') ? entityClass : (typeof entityClass === "function") ? entityClass.name : entityClass.constructor.name;
        const id = `${connectionName}:${entity}`;
        if (this.entity.has(id)) {
            // @ts-ignore
            return this.entity.get(id);
        }
        const container = getRepository<Entity>(entityClass, connectionName);
        this.entity.set(id, container);
        return container;
    }
}
