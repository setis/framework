import "reflect-metadata";
import {EventEmitter} from "events";
import {ConfigSetting} from "../elements/configure";
import {join} from "path";
import App, {ComponentComponent} from "../system";
import * as Table from 'cli-table3'
import TypeOrm from "../components/typeorm";

const notification = new EventEmitter();
notification.setMaxListeners(0);
notification
    .on("error",function(){})
    // .on('error', (...args) => console.error(chalk.keyword('red')('error',...args)))
    // .on('info', (...args) => console.info(chalk.keyword('green')('info',...args)))
    // .on('log', (...args) => console.log(chalk.keyword('white')('log',...args)))
    // .on('debug', (...args) => console.debug(chalk.keyword('cyan')('debug',...args)))
    // .on('warn', (...args) => console.warn(chalk.keyword('orange')('warn',...args)))
const config: ConfigSetting = {
    path: join(__dirname, './resource/config'),
    pattern: [
        `{name}-{env}.{ext}`,
        `{name}.{env}.{ext}`,
        `{name}.{ext}`
    ],
    params: {
        name: 'app'
    },
    env: true
};
const app = new App({
    notification,
    config,
    path: __dirname
});
const tables = () => {
    const table = new Table({
        head: ['name', 'status']
    });
    for (const component of app.components()) {
        try{
            // @ts-ignore
            table.push([component.name.id, component.status.get()]);
        }catch(e){
            console.log(component.name.id)
        }

    }
    for (const module of app.modules()) {
        //@ts-ignore
        table.push([module.name.id, module.status.get()])
    }
    console.log(table.toString());
}
const tables2 = () => {
    const table = new Table({
        head: ['name', 'connection']
    });
        const component = app.use('component://typeorm@0.3.0-alpha.24',true) as ComponentComponent;
        for(let row of (component.api as TypeOrm).status()){
            // @ts-ignore
            table.push(row);
        }
        console.log(table.toString());

}
setInterval(()=>{
    tables();
    // tables2();
},5e3);
(async () => {
    await app.onInit();
    await app.start();
    tables();
    await new Promise(resolve => setTimeout(resolve,2e3));
    tables();
    // await new Promise(resolve => setTimeout(resolve,2e3));
    // app.use('component://controller').command.stop();
    tables();
})();
process
    .on("exit", async code => {
        // await app.stop();
        // await app.onDestroy();
        notification.emit('log', 'process:exit', `exit code:${code} app:destroy`)
    })
    // .on("warning", warning =>{
    //     notification.emit('warn', 'process:warning', warning)
    // })
    // .on("rejectionHandled", async promise => {
    //     if (promise) {
    //         try {
    //             const msg = await promise;
    //             notification.emit('error', 'process:rejectionHandled.resolve', msg);
    //         } catch (e) {
    //             notification.emit('error', 'process:rejectionHandled.reject', e);
    //         }
    //     }
    // })
    // .on("uncaughtException", error => {
    //     notification.emit('error', 'process:uncaughtException', error)
    // })
    // .on("unhandledRejection", async (reason, promise) => {
    //     notification.emit('error', 'process:unhandledRejection', reason);
    //     if (promise) {
    //         try {
    //             const msg = await promise;
    //             notification.emit('error', 'process:unhandledRejection.resolve', msg);
    //         } catch (e) {
    //             notification.emit('error', 'process:unhandledRejection.reject', e);
    //         }
    //
    //     }
    // });
