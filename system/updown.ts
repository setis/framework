import {App} from "./app";
import {Subject, Subscription} from "rxjs";
import {TypeComponent, TypeLevelEnum} from "./interface";
import {NameComponent, NameModule, TypeName} from "./name";
import {filter, map} from "rxjs/operators";

export enum UpDownWatch {
    Down,
    Up

}
const logger = require('debug')('app:updown');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    debug: logger.extend('debug'),
    log: logger.extend('log')
};

/**
 * @description подписаться/отписаться на событие(доступен/не доступен) опреденного компонента
 */
export class UpDown {
    readonly store: Map<string, Subject<any>> = new Map();
    readonly subscription:Subscription = new Subscription();
    constructor(readonly app: App) {

    }

    /**
     * @description получение индификатора(id)
     * @param watch
     * @param value
     * @param strict
     */
    static id(watch:UpDownWatch,value: string | TypeName,strict:boolean = false):string {
        const name = (typeof value === "string") ? value : value.id;
        return `${watch.toString()}:${(strict) ? '1' : '0'}:${name}`;
    }

    protected watch(id:string,value:string | TypeName,strict:boolean,mode:boolean){
        if(this.store.has(id)){
            return this.store.get(id);
        }
        const subject:Subject<TypeComponent> =  new Subject();
        this.store.set(id,subject);
        const name = (typeof value === "string")?this.app.names.use(value):value;
        this.subscription.add(
            this
                .app
                .subject
                .pipe(
                    filter(value1 => {
                        const [component,state] = value1;
                        if(state !== mode){
                            return false;
                        }
                        if(component.name.id === name.id){
                            return true;
                        }
                        if(strict){
                            return false;
                        }
                        switch (name.type) {
                            case TypeLevelEnum.Component:
                                return component.type === name.type && (component.name as NameComponent).module === (name as NameComponent).module;
                            case TypeLevelEnum.Module:
                                return component.type === name.type && (component.name as NameModule).name === (name as NameModule).name;
                            default:
                                log.error(`нет такого типа ${name.type} для ${name.id}`)
                                return false;
                        }
                    }),
                    map(value1 => value1[0])
                )
                .subscribe(subject)


        );
        return subject;
    }

    /**
     * @description получение компонента, событие когда компонента станнет доступен(запустить, возобновиться)
     * @param value
     * @param strict
     */
    up(value:string | TypeName,strict:boolean = false):Subject<TypeComponent>{
        const id = UpDown.id(UpDownWatch.Up, value,strict);
        const subject = this.watch(id,value,strict,true);
        setImmediate(()=>{
            try{
                const result = this.app.use(value,strict);
                subject.next(result);
            }catch (e) {
                log.error(e);
                subject.error(e);
            }
        });
        return subject;
    }

    /**
     * @description событие когда компонент станет недоступен
     * @param value
     * @param strict
     */
    down(value:string | TypeName,strict:boolean = false):Subject<void>{
        const id = UpDown.id(UpDownWatch.Down, value,strict);
        const subject = this.watch(id,value,strict,false);
        setImmediate(()=>{
            try{
                this.app.use(value,strict);
            }catch (e) {
                subject.next();

            }
        });
        return subject;
    }

}
