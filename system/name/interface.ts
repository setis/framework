import {NameComponent} from "./component/component";
import {NameModule} from "./component/module";
import {TypeLevel} from "../interface";

export interface ParametersNameModule {
    name: string;
    version?: string;
}
export type ParametersNameComponent = ParametersNameComponentV1 | ParametersNameComponentV2 ;

export interface ParametersNameComponentV1 {
    component: string;
    module: string;
}
export interface ParametersNameComponentV2 {
    module: string;
    name: string;
    version?: string;
}
export interface ParametersNameId {
    id:string
}

export interface ParametersName extends NameRequired{
    component?: string;
    module?: string;
    name?: string;
    version?: string;
}

export interface NameRequired extends TypeLevel{
    id?: string;
}

export type TypeName = NameComponent | NameModule;
