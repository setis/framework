import {ParametersName, ParametersNameComponent, ParametersNameModule, TypeName} from "./interface";
import {NameComponent, NameModule} from "./component";
import {TypeLevelEnum} from "../interface";

const cloner = (val) => Object.assign( Object.create( Object.getPrototypeOf(val)), val)
export class Name {
    public readonly store: Map<string, TypeName> = new Map();
    private static instance: Name;
    public static getInstance(): Name {
        if (!Name.instance) {
            Name.instance = new Name();
        }

        return Name.instance;
    }
    public static factory(value: string | ParametersName): TypeName {
        let container: TypeName;
        switch (typeof value) {
            case "string":
                const [, module, , component] = /^(?<module>.*?)(:\/\/(?<component>.*?))?$/gmi.exec(value);
                if (component) {
                    container = new NameComponent(value);
                } else if (module) {
                    container = new NameModule(value);
                } else {
                    throw new Error(`ох , бл...`);
                }
                break;
            case "object":
                switch (value.type) {
                    case TypeLevelEnum.Module:
                        container = new NameModule(value as ParametersNameModule);
                        break;
                    case TypeLevelEnum.Component:
                        container = new NameComponent(value as ParametersNameComponent);
                        break;
                    default:
                        throw new Error(`не указана или нет такого значения свойство: ${value.type}`);
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }

        return container;

    }

    static is(value: any): boolean {
        return (value instanceof NameModule || value instanceof NameComponent);
    }

    public add(value: TypeName): this {
        if (!Name.is(value)) {
            throw new Error(`ох , бл...`);
        }
        this.store.set(value.id, value);
        return this;
    }

    public create(value: string | ParametersName): TypeName {
        const container = Name.factory(value);
        if(this.store.has(container.id)){
            return this.store.get(container.id);
        }
        this.store.set(container.id, container);
        return container;
    }

    public remove(value: TypeName | string | ParametersName) {
        let container: TypeName;
        switch (typeof value) {
            case "object":
                container =
                    (Name.is(value))
                        ? value as TypeName
                        : Name.factory(value);
                break;
            case "string":
                container = Name.factory(value);
                break;
            default:
                throw new Error(`ох , бл...`);
        }
        if (this.store.has(container.id)) {
            this.store.delete(container.id);
        }
    }

    public use(name: string|ParametersName,clone:boolean = false): TypeName {
        switch (typeof name) {
            case "string":
                if (this.store.has(name)) {
                    const result = this.store.get(name);
                    return (clone)?cloner(result):result;
                }
                return this.create(name);
            case "object":
                if(Name.is(name)){
                    if(!this.store.has(name.id)){
                        this.store.set(name.id,name as TypeName);
                    }
                    return (clone)?cloner(name):name;
                }
                const result = this.create(name);
                return (clone)?cloner(result):result;
            default:
                throw new Error(`ох , бл...`);
        }
    }
}
