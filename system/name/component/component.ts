import {satisfies} from "semver";

import {NameRequired, ParametersName, ParametersNameComponent} from "../interface";
import {TypeLevelEnum} from "../../interface";

export class NameComponent implements ParametersName, NameRequired  {
    readonly type: TypeLevelEnum = TypeLevelEnum.Component;

    public get component(): string {
        return this._component;
    }

    public set component(value: string) {
        this._component = value;
        const [name, version] = value.split("@");
        this._name = name;
        this._version = version;
        delete this._id;
    }

    public get id(): string {
        if (!this._id) {
            this.build_id();
        }

        return this._id;
    }

    public set id(value: string) {
        this._id = value;
        this.parse_id();
    }

    public get module(): string {
        return this._module;
    }

    public set module(value: string) {
        this._module = value;
        delete this._id;

    }

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
        this.build_component();
        delete this._id;
    }

    public get version(): string {
        return this._version;
    }

    public set version(value: string) {
        this._version = value;
        this.build_component();
        delete this._id;
    }

    private _component: string;

    private _id: string;

    private _module: string;

    private _name: string;

    private _version?: string;

    public constructor(value?: string|ParametersNameComponent) {
        if (value) {
            this.parse(value);
        }
    }

    public check(): boolean;
    public check(version: string): boolean;
    public check(version?: string): boolean {
        if (this.version === version || version === undefined || version === "*") {
            return true;
        }

        return satisfies(version, this.version);
    }

    public load(value: ParametersNameComponent) {
        const validator = (value1)=>{
            return (value1 === "version")
                ? ["string", "undefined"].includes(typeof value[value1])
                : value.hasOwnProperty(value1) && typeof value[value1] === "string"
        };
        const list1 = [
            "module",
            "name",
            "version"
        ];
        const valid1 = list1.filter(validator);
        if (valid1.length>0 && valid1.includes("module") && valid1.includes("name")) {
            valid1.forEach((value1) => this[value1] = value[value1]);
            this.build_id();
            return;
        }
        const list2 = [
            "module",
            "component",
        ];
        const valid2 = list2.filter(validator);
        if (valid2.length > 0 && valid2.includes("module") && valid2.includes("component")) {
            valid2.forEach((value1) => this[value1] = value[value1]);
            this.build_id();
            return;
        }
        throw new Error(`${valid1.join(",")} or ${valid2.join(",")}`);

    }

    public parse(value: ParametersNameComponent | string) {
        switch (typeof value) {
            case "object":
                this.load(value);
                break;
            case "string":
                this.id = value;
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    public toString() {
        return this.id;
    }

    protected build_component() {
        this._component = (this.version)
            ? `${this.name}@${this.version}`
            : this.name;
    }

    protected build_id() {
        this._id = `${this.module}://${this.component}`;
    }

    protected parse_id() {
        [
            "module",
            "name",
            "version",
            "component",
        ]
            .forEach((value1) => delete this[`_${value1}`]);
        [, this.module, , this.component] = /^(?<module>.*?)(:\/\/(?<component>.*?))?$/gmi.exec(this._id);
    }
}
