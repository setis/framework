import {satisfies} from "semver";

import {NameRequired, ParametersNameModule} from "../interface";
import {TypeLevelEnum} from "../../interface";

export class NameModule implements ParametersNameModule, NameRequired {
    readonly type: TypeLevelEnum = TypeLevelEnum.Module;

    public constructor(value?: string | ParametersNameModule) {
        if (value) {
            this.parse(value);
        }
    }

    private _id: string;

    public get id(): string {
        if (!this._id) {
            this.build_id();
        }

        return this._id;
    }

    public set id(value: string) {
        this._id = value;
        this.parse_id();
    }

    private _name: string;

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
        delete this._id;
    }

    private _version?: string;

    public get version(): string {
        return this._version;
    }

    public set version(value: string) {
        this._version = value;
        delete this._id;
    }

    public check(): boolean;
    public check(version: string): boolean;
    public check(version?: string): boolean {
        if (this.version === version || version === undefined || version === "*") {
            return true;
        }

        return satisfies(version, this.version);
    }

    public load(value: ParametersNameModule) {
        if (typeof value.name !== "string") {
            throw new Error(`name не может типом кроме string`);
        }
        const {name, version} = value;
        this.name = name;
        this.version = version;
    }

    public parse(value: ParametersNameModule | string) {
        switch (typeof value) {
            case "object":
                this.load(value);
                break;
            case "string":
                this.id = value;
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    public toString() {
        return this.id;
    }

    protected build_id() {
        this._id = (this.version)
            ? `${this.name}@${this.version}`
            : `${this.name}`;
    }

    protected parse_id() {
        [
            "name",
            "version",
        ]
            .forEach((value) => delete this[`_${value}`]);
        const [, module] = /^(?<module>.*?)(:\/\/(?<component>.*?))?$/gmi.exec(this._id);
        this._id = module;
        [this._name, this._version] = module.split("@");
        this.load(this);
    }

}
