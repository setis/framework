import 'reflect-metadata';
import {App} from "./app";

export default App
export * from './name'
export * from './config'
export * from './dependence'
export * from './env'
export * from './app'
export * from './status'
export * from './command'
export * from './module'
export * from './component'
export * from './interface'
