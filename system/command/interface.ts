
export enum CommandEnum {
    None = 'none',
    //package
    Install = 'install',
    Uninstall = 'uninstall',
    // manager
    Start = "start",
    Stop = "stop",
    Restart = "restart",
    // reload
    /**
     * @description Горячая перезагрузка обновляет только те файлы, которые были изменены без потери состояния приложения. Например, если вы были в четырех ссылках глубоко в вашей навигации и сохранили изменения в каком-то стиле, состояние не изменилось бы, но новые стили появятся на странице без необходимости перехода на страницу, на которой вы находитесь, потому что вы все равно будете быть на одной странице.
     */
    HotReload = "hot reload",
    /**
     * @description Живая перезагрузка перезагружает или обновляет все приложение при изменении файла. Например, если вы были в четырех ссылках глубоко в своей навигации и сохранили изменения, живая перезагрузка перезапустила приложение и загрузила приложение обратно на начальный маршрут.
     */
    LiveReload = "live reload",
    Reload = "reload",
    // zombie
    Sleep = "sleep",
    WakeUp = "wakeup",
    // pause
    Pause = "pause",
    Resume = "resume",
    // backup
    Restore = "restore",
    Dump = "dump",

    //api
    UpApi = "up api",
    DownApi = "down api"

}

export const CommandGroup: {
    [group: string]: string[];
} = {
    none:[
        CommandEnum.None
    ],
    package:[
        CommandEnum.Install,
        CommandEnum.Uninstall
    ],
    manager: [
        CommandEnum.Start,
        CommandEnum.Stop,
        CommandEnum.Restart,
    ],
    zombie: [
        CommandEnum.Sleep,
        CommandEnum.WakeUp,
    ],
    reload: [
        CommandEnum.HotReload,
        CommandEnum.LiveReload,
        CommandEnum.Reload,
    ],
    pause: [
        CommandEnum.Pause,
        CommandEnum.Resume,
    ],
    backup: [
        CommandEnum.Dump,
        CommandEnum.Restore,
    ],
    api: [
        CommandEnum.UpApi,
        CommandEnum.DownApi
    ]
};
export const CommandList: string[] = [].concat(...Object.keys(CommandGroup).map((value) => CommandGroup[value]));
