import {CommandModule} from "./module";
import {CommandComponent} from "./component";

export * from './module'
export * from './component'
export * from './base'
export * from './interface'

export type Command = CommandModule | CommandComponent;
