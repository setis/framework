import {CommandBase} from "./base";
import {ComponentComponent} from "../component";

const logger = require('debug')('system:command:component');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
export class CommandComponent extends CommandBase {

    constructor(readonly container: ComponentComponent) {
        super();
        log.debug(`class: ${this.constructor.name}   container: ${this.container.constructor.name}` )
    }

    get app() {
        return this.container.app;
    }
}
