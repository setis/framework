import {CommandEnum, CommandGroup, CommandList} from "./interface";
import {Subject, Subscription} from "rxjs";
import {RetryIntricate} from "../../elements/retry";
import {StatusEnum} from "../status";
import {App} from "../app";
import {OnDestroy, OnInit, TypeComponent} from "../interface";
import {distinctUntilChanged, filter} from "rxjs/operators";

const logger = require('debug')('system:command:base');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
/**
 * @description комманды, хуки на комманды
 */
export abstract class CommandBase implements OnInit, OnDestroy {
    groups:{
        [group: string]: string[];
    } = CommandGroup;
    list: string[] = CommandList;
    readonly subscription: Subscription = new Subscription();
    readonly retry: RetryIntricate = new RetryIntricate();
    abstract app: App;
    abstract container: TypeComponent | any;
    protected command: string = CommandEnum.None;
    mode_retry:boolean = false;
    get instance() {
        return this.container.instance;
    }

    get status() {
        return this.container.status;
    }
    readonly subject: Subject<string> = new Subject();

    public async down(value: string) {
        if (this.command !== value) {
            return;
        }
        await this
            .subject
            .pipe(
                filter((value1) => value1 !== value),
            )
            .toPromise();
    }

    public get(): string {
        return this.command;
    }

    public group(value: string = this.command): string | void {
        for (const group in this.groups) {
            if (this.groups[group].includes(value)) {
                return group;
            }
        }
    }


    public set(value: string) {
        if (!this.list.includes(value)) {
            throw new Error(`нет такой комманды ${value}`);
        }
        if(this.command !== value){
            this.command = value;
            this.subject.next(value);
        }

    }

    public toString() {
        return this.command;
    }

    public async up(value: string) {
        if (this.command === value) {
            return;
        }
        await this
            .subject
            .pipe(
                filter((value1) => value1 === value),
            )
            .toPromise();
    }
    onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription);
        }
    }

    onInit() {
        if (this.subscription) {
            this.subscription
                .add(
                this.subject.subscribe(
                    value => this.app.command.next([this.container.name.id, value]),
                )
            )

            setTimeout(()=>{
                this.subscription.add(
                    this.container
                        .status
                        .subject
                        .pipe(
                            distinctUntilChanged()
                        )
                        .subscribe(value => {
                            log.debug(value)
                        })
                );
            },150)
            this.subject.subscribe(value => this.run(value));
            this.app.subscription.add(this.subscription);
        }
        const handler = (name: string) => this.handler(name).bind(this);
        this.retry.label = function (this: RetryIntricate, name: string) {
            if (this.store.has(name)) {
                return this.store.get(name);
            }
            const container = handler(name);
            this.store.set(name, container);
            return container;
        };
    }

    /**
     * @description выполнение комманды
     * @param value
     */
    async run(value: string = this.command) {
        log.debug(`action: run class: ${this.constructor.name} emit: ${value} func: ${this.handler(value).name}`);
        if(this.mode_retry){
            await this.exec_retry(value)
        }else{
            await this.exec_noretry(value)
        }

    }


    /**
     * @description выполнение комманды без повтора, вслучае ошибки
     * @param value
     */
    async exec_noretry(value: string) {
        const func = this.handler(value);
        await func.call(this);
    }

    /**
     * @description выполнение комманды с повтором, вслучае ошибки
     * @param value
     */
    async exec_retry(value: string) {
        if (value === CommandEnum.None) {
            this.retry.reset();
            return;
        }
        await this.retry.run(value)
    }

    /**
     * @description список всех комманд на которые есть хуки
     */
    commands(): string[] {
        return Object.keys(this)
            .filter(value => /^run_(.*?)$/gmi.test(value))
            .map(value => /^run_(.*?)$/gmi.exec(value)[1]);
    }

    start() {
        this.set(CommandEnum.Start);

    }

    stop() {
        this.set(CommandEnum.Stop)
    }

    install() {
        this.set(CommandEnum.Install)
    }

    uninstall() {
        this.set(CommandEnum.Uninstall)
    }

    /**
     * @description хук комманды на установку
     */
    async run_install() {
        log.debug(`action: run_install id: ${this.container.name.id} state: ${this.container.state} class: ${this.constructor.name}  container: ${this.container.constructor.name}` )
        if (!this.container.state) {
            await this.container.start();
        }
        if (this.status.isEmpty() || (this.status.get() !== StatusEnum.Init && this.status.lt(StatusEnum.Init))) {
            this.status.set(StatusEnum.Init);
        }
    }

    /**
     * @description хук комманды на зависимости
     */
    async run_dependence(){
        if(!this.container.dependence.state){
            if (!this.container.dependence.depends(true)) {
                this.status.set(StatusEnum.WaitDependencies);
                await this.container.dependence.up();
            }
        }
        if (this.status.isEmpty() || (this.status.get() !== StatusEnum.Dependencies && this.status.lt(StatusEnum.Dependencies))) {
            this.status.set(StatusEnum.Dependencies);
        }
    }
    /**
     * @description хук комманды на
     */
    async run_uninstall() {
        if (this.container.state) {
            if (this.instance.state) {
                await this.run_stop();
            }
            await this.container.uninstall();
        }
        if (this.status.isEmpty() || (this.status.get() !== StatusEnum.Destroy && this.status.lt(StatusEnum.Destroy)) ){
            this.status.set(StatusEnum.Destroy);
        }
    }
    status_start(){

    }
    /**
     * @description хук комманды на запуск
     */
    async run_start() {
        log.debug(`action: run_start  id: ${this.container.name.id}  class: ${this.constructor.name}container: ${this.container.constructor.name}` )
        let i = 0;
        try {
            await this.run_install();
            i++;
            await this.run_dependence();
            i++;
            await this.instance.start();
        } catch (e) {

            switch (i) {
                case 0:
                    throw e;
                case 1:
                    return this.status.set(StatusEnum.Init);
                case 2:
                    return this.status.set(StatusEnum.Dependencies);
                default:
                    throw new Error(`run_start i: ${i} error: ${e.message}`);
            }
        }
        if (this.status.get() !== StatusEnum.Online ) {
            this.status.set(StatusEnum.Online);
        }
    }
    /**
     * @description хук комманды на остановку
     */
    async run_stop() {
        if (this.container.state) {
            if (this.instance.state) {
                await this.instance.stop();
            }
        }
        if (this.status.isEmpty() || (this.status.get() !== StatusEnum.Offline && this.status.gt(StatusEnum.Offline))) {
            this.status.set(StatusEnum.Offline);
        }
    }
    /**
     * @description хук комманды на перезапуск
     */
    async run_restart() {
        await this.run_stop();
        await this.run_start();
    }
    /**
     * @description получение хука на опреденную комманду
     */
    protected handler(name: string) {
        const func = `run_${name}`;
        if (typeof this[func] !== "function") {
            throw new Error(`not found  handler: ${func} for command: ${name}`);
        }
        return this[func]
    }
}
