import {CommandBase} from "./base";
import {ModuleComponent} from "../module";
import {App} from "../app";
import {OnDestroy, OnInit} from "../interface";

const logger = require('debug')('system:command:module');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
export class CommandModule extends CommandBase implements OnInit, OnDestroy {
    constructor(readonly container: ModuleComponent) {
        super();
        log.debug(`class: ${this.constructor.name} container: ${this.container.constructor.name}` )
    }
    get instance() {
        return this.container.api;
    }
    get app(): App {
        return this.container.app
    }
}
