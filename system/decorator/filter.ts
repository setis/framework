export type ParametersList = (((k: string, v: any) => boolean) | string | RegExp);

export function Filter(data: {
    excludes?: ParametersList[],
    includes?: ParametersList[]
}) {
    const handler = (func: ParametersList, mode: boolean = true) => {
        let call;
        switch (typeof func) {
            case "function":
                call = func;
                break;
            case "string":
                call = (k: string, v: any) => func == k;
                break;
            case "object":
                if (!(func instanceof RegExp)) {
                    throw new Error();
                }
                call = (k: string) => func.test(k);
                break;
            default:
                throw new Error();
        }
        return (mode) ? call : (...args) => !call.apply(null, ...args);

    };
    return (target: any) => {
        const result = new Array();
        for (const propertyName of Object.keys(target.prototype)) {
            const descriptor = Object.getOwnPropertyDescriptor(target.prototype, propertyName);
            result.push(propertyName, descriptor);
        }
        const {excludes = [], includes = []} = data;
        if (excludes.length === 0 && includes.length === 0) {
            return Array.from(result.keys());
        }
        const i = includes.map(value => handler(value, true));
        const e = excludes.map(value => handler(value, false));
        return result
            .filter(value => {
                for (const h of e) {
                    if (!h(...value)) {
                        for (const h of i) {
                            return h(...value);
                        }
                    }
                }

            })
            .map(value => value[0])


    }
}
