import {Reductor} from "./reductor";
import {Filter} from "./filter";

export interface OnInitialization {
    initialization:boolean;
    state:boolean;
}
const filter = {

    includes:[
        (k:string,v:any)=> v instanceof Function
    ],
    excludes:[
        'initialization',
        'onInit',
        'onDestroy',
        'state',
        'start',
        'stop',
        'container',
        'notification',
        'run',
        'subject',
        'subscription',
        'store',
        'parameters',
        (k:string,v:any)=> (v instanceof Function) && !(v.constructor && v.constructor.name)
    ]
}
export function Run<T extends OnInitialization = any>(): ClassDecorator {

    return (target:any)=>{
        const list = Filter(filter)(target)
        Reductor({
            property:'initialization',
            up: 'onInit',
            down: 'onDestroy',
            list
        })(target);
        Reductor({
            property:'state',
            up: 'run',
            list
        })(target);
    }

}

export function Boot<T extends OnInitialization = any>(): ClassDecorator {
    return (target:any)=>{
        const list = Filter(filter)(target)
        Reductor({
            property:'initialization',
            up: 'onInit',
            down: 'onDestroy',
            list
        })(target);
        Reductor({
            property:'state',
            up: 'start',
            down: 'stop',
            list
        })(target);
    }

}
