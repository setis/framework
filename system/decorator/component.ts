// import {StatusEnum} from "../status";
// import {CommandEnum} from "../command";
// import {ParametersPackageInfo, TypeLevelEnum} from "../interface";
// import {ComponentComponent, ComponentModule} from "../component";
//
// export type Match = string | ((method: string) => boolean) | RegExp;
//
// export interface ParametersBase {
//     config?: {
//         type: 'fs',
//         path: string;
//     } | {
//         type: 'data',
//         data: any
//     } | {
//         type: 'parent',
//         parent: string
//     };
//     status: {
//         [name: string]: number;
//     };
//     command: {
//         [name: string]: string
//     };
//     type: TypeLevelEnum;
//     init: {
//         var?: string      //initialization
//         up?: string      //'onInit'
//         down?: string    //'onDestroy'
//         include?: Match[],
//         exclude?: Match[]
//     }
//     state: {
//         var?: string      //state
//         up?: string      //'start',
//         down?: string    //'stop',
//         include?: Match[],
//         exclude?: Match[]
//     }
//
// }
//
//
// const _default: ParametersBase = {
//     status: {
//         [StatusEnum.Unregister]: -1000,
//         [StatusEnum.Destroy]: -900,
//         [StatusEnum.Dependents]: -800,
//         [StatusEnum.WaitDependents]: -700,
//         [StatusEnum.Offline]: -100,
//
//         [StatusEnum.None]: 0,
//
//         [StatusEnum.Register]: 100,
//         [StatusEnum.Init]: 200,
//         [StatusEnum.WaitDependencies]: 300,
//         [StatusEnum.Dependencies]: 400,
//         [StatusEnum.Online]: 1000,
//     },
//     command: {
//         [CommandEnum.Install]: StatusEnum.Init,
//         [CommandEnum.Uninstall]: StatusEnum.Destroy,
//         [CommandEnum.Stop]: StatusEnum.Offline,
//         [CommandEnum.Start]: StatusEnum.Online,
//     },
//     type: TypeLevelEnum.Component,
//     state: {
//         var: 'state',
//         down: 'start',
//         up: 'stop',
//         include: [
//             '*'
//         ]
//     },
//     init: {
//         var: 'initialization',
//         up: 'onInit',
//         down: 'onDestroy'
//     }
// };
//
// export function Component() {
//     return function (target):ClassDecorator {
//         return function () {
//             const instance = Reflect.construct(target, arguments);
//             const component =  new ComponentComponent({
//                 ...arguments as any,
//                 api:instance
//             });
//             instance.api = component;
//             return instance;
//         }
//     }
// }
