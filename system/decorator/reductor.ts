
export interface ParametersReductor {
    /**
     * @description методы без проверки иницилизацию не будет запускаться
     * @default ['start','stop','restart']
     */
    list?:string[]
    /**
     * @description метод иницилизации
     * @default onInit
     */
    up?:string;
    /**
     * @description метод деинитилизации
     * @default onDestroy
     */
    down?:string;
    /**
     * @description свойство состояние иницилизации
     * @default initialization
     */
    property?:string;


}

function upReductor(constructor: any,property:string, method: string) {
    if (constructor.prototype[method]) {
        constructor.prototype[method] = new Proxy(constructor.prototype[method], {
            async apply(target: any, thisArg: any, argArray?: any) {
                try{
                    const result = await target.apply(thisArg, argArray);
                    thisArg[property] = true;
                    return result;
                }catch (e) {
                    throw e;
                }
            }
        });
    }
}

function downReductor(constructor: any,property:string, method: string) {
    if (constructor.prototype[method]) {
        constructor.prototype[method] = new Proxy(constructor.prototype[method], {
            async apply(target: any, thisArg: any, argArray?: any) {
                if (thisArg) {
                    const result = await target.apply(thisArg, argArray);
                    thisArg[property] = false;
                    return result;
                }
            }
        });
    }
}

function execReductor(constructor: any,property:string, up: string, method: string) {
    if (constructor.prototype.hasOwnProperty(method)) {
        constructor[method] = new Proxy(constructor.prototype[method], {
            async apply(target: any, thisArg: any, argArray?: any) {
                if (!thisArg[property]) {
                    await thisArg[up]();
                }
                return await target.apply(thisArg, argArray);
            }
        });
    }
}
export function Reductor(parameters:ParametersReductor){
    const {down,list,property,up} = parameters;
    return (constructor: any) => {
        if (list) {
            if (list instanceof Array) {
                for (const method of list) {
                    execReductor(constructor,property, up, method);
                }
            } else if (typeof list === "string") {
                execReductor(constructor,property, up, list);
            }
        }
        if (!constructor.hasOwnProperty(property)) {
            constructor[property] = false;
        }
        if (up) {
            upReductor(constructor, property,up);
        }
        if (down) {
            downReductor(constructor, property,down)
        }
    }
}
