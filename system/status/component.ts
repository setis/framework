import {StatusBase} from "./base";
import {ComponentComponent} from "../component";


export class StatusComponent extends StatusBase {
    constructor(readonly container: ComponentComponent) {
        super();
    }

    get app() {
        return this.container.app;
    }

}
