import {ModuleComponent} from "../module";
import {OnDestroy, OnInit} from "../interface";
import {StatusBase} from "./base";

export class StatusModule extends StatusBase implements OnInit, OnDestroy {

    constructor(readonly container: ModuleComponent) {
        super();
    }

    get app() {
        return this.container.app;
    }

}
