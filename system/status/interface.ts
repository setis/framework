export enum StatusEnum {
    None = 'none',
    // registry
    Register = "register",
    Unregister = "unregister",
    // app
    Init = "init",
    Destroy = "destroy",
    // mode
    Online = "online",
    Offline = "offline",
    // dependence
    Dependents = "dependents",
    WaitDependents = "wait dependents",
    Dependencies = "dependencies",
    WaitDependencies = "wait dependencies",
    //api
        //connect listen
    UpApi = "up api",
        //close exit
    DownApi = "down api",

    // zombie
    Sleep = "sleep",
    WakeUp = "wakeup",
    // pause
    Pause = "pause",
    Resume = "resume",
    // backup
    Restore = "restore",
    Dump = "dump",

}

export const StatusGroup: { [name: string]: string[] } = {
    registry: [
        StatusEnum.Register,
        StatusEnum.Unregister,
    ],
    app: [
        StatusEnum.Init,
        StatusEnum.Destroy,
    ],
    dependence: [
        StatusEnum.Dependents,
        StatusEnum.Dependencies,
        StatusEnum.WaitDependents,
        StatusEnum.WaitDependencies,
    ],
    mode: [
        StatusEnum.Online,
        StatusEnum.Offline,
    ],
    zombie: [
        StatusEnum.Sleep,
        StatusEnum.WakeUp,
    ],
    pause: [
        StatusEnum.Pause,
        StatusEnum.Resume,
    ],
    backup: [
        StatusEnum.Dump,
        StatusEnum.Restore,
    ],
    api: [
        StatusEnum.UpApi,
        StatusEnum.DownApi
    ]
};
export const StatusList: string[] = [].concat(...Object.keys(StatusGroup).map((value) => StatusGroup[value]));

export const StatusWeights: { [status: string]: number } = {
    [StatusEnum.Register]: 100,
    [StatusEnum.Init]:200,
    [StatusEnum.WaitDependencies]: 300,
    [StatusEnum.Dependencies]: 400,
    [StatusEnum.WakeUp]: 500,
    [StatusEnum.Restore]:500,
    [StatusEnum.Resume]:500,
    [StatusEnum.UpApi]:500,
    [StatusEnum.Online]:1000,

    [StatusEnum.Offline]:-100,
    [StatusEnum.Sleep]:-100,
    [StatusEnum.Dump]:-100,
    [StatusEnum.Pause]:-100,
    [StatusEnum.DownApi]:-100,
    [StatusEnum.WaitDependents]:-300,
    [StatusEnum.Dependents]:-400,

    [StatusEnum.Destroy]:-900,
    [StatusEnum.Unregister]:-1000,


}
