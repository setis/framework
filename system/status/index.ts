import {StatusModule} from "./module";
import {StatusComponent} from "./component";

export * from './module'
export * from './component'
export * from './interface'
export * from './base'

export type Status = StatusModule | StatusComponent;
