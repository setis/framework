import {Subject, Subscription} from "rxjs";
import {StatusEnum, StatusWeights} from "./interface";
import {App} from "../app";
import {OnDestroy, OnInit} from "../interface";
import {filter} from "rxjs/operators";

const logger = require('debug')('system:status:base');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
export abstract class StatusBase implements OnInit, OnDestroy {
    weights:  { [status: string]: number } = StatusWeights;
    protected _offline: string = StatusEnum.Offline;
    protected _online: string = StatusEnum.Online;
    abstract app: App;
    container:any;
    readonly subscription: Subscription = new Subscription();
    readonly subject: Subject<string> = new Subject();
    status: string = this._offline;

    get online(): string {
        return this._online;
    }

    set online(value: string) {
        if (!this.has(value) || this.weights[value] < 0) {
            throw new Error(`ох , бл...`);
        }
        this._online = value;
    }

    public get offline(): string {
        return this._offline;
    }

    public set offline(value: string) {
        if (!this.has(value) || this.weights[value] > 0) {
            throw new Error(`ох , бл...`);
        }
        this._offline = value;
    }

    list(): string[] {
        return Object.keys(this.weights);
    }

    offline_list(value: false): IterableIterator<string>
    offline_list(value: true): IterableIterator<number>
    * offline_list(value: boolean = false): IterableIterator<string | number> {
        for (let k in this.weights) {
            let v = this.weights[k];
            if (v < 0) {
                yield (value) ? v : k;
            }
        }
    }

    online_list(value: false): IterableIterator<string>
    online_list(value: true): IterableIterator<number>
    * online_list(value: boolean = false): IterableIterator<string | number> {
        for (let k in this.weights) {
            let v = this.weights[k];
            if (v > 0) {
                yield (value) ? v : k;
            }
        }
    }

    public async down(value: string = this.online) {
        if (this.status !== value) {
            return;
        }
        await this
            .on()
            .pipe(
                filter((value1) => value1 !== value),
            )
            .toPromise();
    }

    public emit(value: string) {
        this.subject.next(value);
    }

    public get(): string {
        return this.status;
    }
    mode(): string;
    mode(bool: true): boolean;
    mode(bool: true, value: string): boolean;
    mode(bool: false): string;
    mode(bool: false, value: string): string;
    mode(bool: boolean = false, value: string = this.status): string | boolean {
        if (!this.has(value)) {
            throw new Error(`нет такого в списке статуса: ${value}`);
        }
        const mode = (this.weights[value] > 0);
        return (bool)
            ? mode
            : (mode) ? this.online : this.offline;
    }
    isEmpty():boolean{
        return (this.status === undefined);
    }
    gt(value: string) {
        const current = this.current(this.status);
        const next = this.current(value);
        log.debug(`class ${this.constructor.name} action: gt status: ${this.status} value: ${value} current: ${current} value: ${next} result: ${(current > next)}`)
        return (current > next);
    }
    gte(value: string) {
        const current = this.current(this.status);
        const next = this.current(value);
        log.debug(`class ${this.constructor.name} action: gte status: ${this.status} value: ${value} current: ${current} value: ${next} result: ${ (current > next || current === next)}`)
        return (current > next || current === next);
    }

    lt(value: string) {
        const current = this.current(this.status);
        const prev = this.current(value);
        log.debug(`class ${this.constructor.name} action: lt status: ${this.status} value: ${value} current: ${current} value: ${prev} result: ${(current < prev)}`)
        return (current < prev);
    }
    lte(value: string) {
        const current = this.current(this.status);
        const prev = this.current(value);
        log.debug(`class ${this.constructor.name} action: lte status: ${this.status} value: ${value} current: ${current} value: ${prev} result: ${(current < prev || current === prev)}`)
        return (current <  prev || current === prev);
    }
    eq(value: string) {
        const current = this.current(this.status);
        const next = this.current(value);
        log.debug(`class ${this.constructor.name} action: eq status: ${this.status} value: ${value} current: ${current} value: ${next} result: ${(current === next)}`)
        return (current === next);
    }

    has(value: string): boolean {
        return this.weights.hasOwnProperty(value)
    }

    current(value: string = this.status): number {
        if (!this.has(value)) {
            throw new Error(`нет такого статуса: ${value}`);
        }
        return this.weights[value];
    }

    isLast(value: string): boolean {
        const i = this.current(value)
        const f = (i > 0);
        const last = this.last(f);
        return (last === i);
    }

    isFirst(value: string): boolean {
        const i = this.current(value)
        const f = (i > 0);
        const first = this.first(f);
        return (first === i);
    }

    last(on: boolean) {
        const list = (on) ? this.online_list(true) : this.offline_list(true);
        return Math.max.apply(Math, Array.from(list))
    }

    first(on: boolean) {
        const list = (on) ? this.online_list(true) : this.offline_list(true);
        return Math.min.apply(Math, Array.from(list))
    }

    next(value: string): number {
        const i = this.current(value)
        const on = (i > 0);
        const list = Array.from((on) ? this.online_list(true) : this.offline_list(true)).sort(function (a, b) {
            return a - b;
        });
        const limit = Math.max.apply(Math, list);
        if (limit === i) {
            return i;
        }
        for (let weight of list) {
            if (weight > i) {
                return weight;
            }
        }
    }

    public on() {
        return this.subject;
    }

    public prev(value: string): number {
        const i = this.current(value)
        const on = (i > 0);
        const list = Array.from((on) ? this.online_list(true) : this.offline_list(true)).sort(function (a, b) {
            return b - a;
        });
        const limit = Math.min.apply(Math, list);
        if (limit === i) {
            return i;
        }
        for (let weight of list) {
            if (weight < i) {
                return weight;
            }
        }
    }

    public set(value: string) {
        if (!this.has(value)) {
            throw new Error(`не существует такой статус ${value}`);
        }
        this.status = value;
        this.emit(value);
    }

    public toString() {
        return this.status;
    }

    public async up(value: string = this.online) {
        if (this.status === value) {
            return;
        }
        await this
            .on()
            .pipe(
                filter((value1) => value1 === value),
            )
            .toPromise();
    }
    async onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription);
        }
    }

    async onInit() {
        if (this.subscription) {
            this.subscription.add(
                this.subject.subscribe(value => this.app.status.next([this.container.name.id, value]))
            );
            this.app.subscription.add(this.subscription);
        }
    }

}
