import {LoaderOnce, LoaderOnceConfigure} from "../../bases/loader";
import {Boot, OnInitialization} from "../decorator";
import {App} from "../app";
import {Subject, Subscription} from "rxjs";
import {PathComponent} from "../../elements/path";
import {ConfigProperty} from "../../elements/configure";
import {EventEmitter} from "events";
import {RegistryOnce} from "../../elements/registry";
import {NameModule} from "../name/component";
import {ParametersNameModule} from "../name";
import {ModuleComponent} from "./component";
import {ModeModule} from "../mode";
import {Notification, PackageInfo, ParametersPackageMeta, TypeLevel, TypeLevelEnum} from "../interface";
import {StatusEnum} from "../status";

const logger = require('debug')('system:module:module');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
@Boot()
export class ModuleModule extends LoaderOnce implements OnInitialization, TypeLevel {
    readonly type: TypeLevelEnum = TypeLevelEnum.App;
    initialization: boolean;
    state: boolean = false;
    public container: App;
    public readonly store: Map<string, ModuleComponent> = new Map();
    readonly subject: Subject<[string, boolean]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    mode: ModeModule;
    public constructor(readonly parameters: {
        config?: ConfigProperty<LoaderOnceConfigure> | string;
        notification?: EventEmitter | Notification;
        container: App;
        path?: PathComponent;
    }) {
        super();
    }

    public [Symbol.iterator]() {
        return this.store[Symbol.iterator]();
    }

    public values() {
        return this.store.values();
    }

    public entries() {
        return this.store.entries();
    }

    public has(name: string) {
        return this.store.has(name);
    }

    public keys() {
        return this.store.keys();
    }

    public async handler(data: any, path: string): Promise<string> {
        const {meta, api} = this.handler_package(data);
        // @ts-ignore
        const container = new api(this, meta);
        log.info(`action: handler name: ${meta.name} instance: ${api.name}`);
        this.register(container);

        return container.name.id;
    }

    async start() {
        if (!this.state) {
            log.info('action: start')
            await this.run();
            await this.mode.start();
            this.state = true;
        }
    }

    public async onDestroy() {
        const tasks = [];
        for (const name of this.store.keys()) {
            tasks.push(this.unregister(name));
        }
        await Promise.all(tasks);
        await this.mode.onDestroy();
        this.destroySubscription();
    }

    protected async destroySubscription(){
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.container.subscription.remove(this.subscription);
        }
    }

    async onInit() {
        const {config, notification, container, path} = this.parameters;
        if (container) {
            if (container.type !== TypeLevelEnum.App) {
                throw new Error(`not found parameter:container`);
            }
            this.container = container;
        }
        await this.initConfig();
        this.initNotification();
        this.initPath();
        this.initRegistry();
        this.initPackage();
        await this.initSubscription();
        await this.initMode();

    }
    protected async initConfig(){
        if(!this.config){
            const {config} = this.parameters;
            if (config instanceof ConfigProperty) {
                this.config = config;
            }
            else {
                throw new Error(`ох , бл...`);
                // this.config = this.container.config.use(`loader`)
            }
        }
    }
    protected initPath(){
        if(!this.path){
            const {path} = this.parameters;
            if (path) {
                switch (typeof path) {
                    case "string":
                        this.path = new PathComponent(path);
                        break;
                    case "object":
                        if (path instanceof PathComponent) {
                            this.path = path;
                        }
                        break;
                    default:
                        throw new Error(`ох , бл...`);
                }
            }else{
                this.path = new PathComponent(this.container.path)
            }
            this.path.load(this.config.get("loader.path", []));
        }
    }
    protected initPackage(){
        if(!this.package){

            this.package = this.config.get("loader.package", []);
        }
    }
    protected initNotification(){
        if(!this.notification){
            const {notification} = this.parameters;
            const id = `system://modules`;
            if (typeof notification === "function") {
                this.notification = notification;
            } else if (notification instanceof EventEmitter) {
                this.notification = (type, call, ...args) => notification.emit(type, `${id}#${call}`, ...args);
            } else {
                this.notification = (type, call, ...args) => this.container.notification.emit(type, `${id}#${call}`, ...args);
            }
        }
    }
    protected initRegistry(){
        if (!this.registry) {
            this.registry = new RegistryOnce();
            this.registry.root = this.path.root;
            this.registry.load(this.config.get("loader.registry", {}));
        }
    }
    protected async initSubscription(){
        if (this.subscription) {
            this.subscription
                .add(
                    this.subject.subscribe(value => {
                        let [id, state] = value;
                        this.container.status.next([id,  (state) ? StatusEnum.Register : StatusEnum.Unregister])
                    })
                );

            this.container.subscription.add(this.subscription);
        }
    }
    protected async initMode(){
        if(!this.mode){
            this.mode = new ModeModule(this);
            await this.mode.onInit();
        }
    }
    register(value: ModuleComponent) {
        if (!(value instanceof ModuleComponent)) {
            const error = new Error(`не является ModuleComponent`);
            this.notification("error", "register", error);
            throw error;
        }
        const id = value.name.id;
        this.store.set(id, value);
        this.subject.next([id, true]);
    }

    async unregister(value: ModuleComponent | string | NameModule | ParametersNameModule) {
        const remove = async (value: string) => {
            const container = this.store.get(value);
            const id = container.name.id;
            if (this.container.state) {
                await container.stop();
            }
            await container.onDestroy();
            this.store.delete(value);
            this.subject.next([id, false]);
        };
        switch (typeof value) {
            case "string":
                if (this.store.has(value)) {
                    await remove(value);
                }
                break;
            case "object":
                if (value instanceof NameModule) {
                    await remove(value.id);
                } else if (value instanceof ModuleComponent) {
                    await remove(value.name.id);
                } else {
                    const name = new NameModule(value as ParametersNameModule);
                    await remove(name.id);
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    async stop() {
        log.info('action: stop')
        if (!this.state) {
            return;
        }
        await this.mode.stop();
        const remove = async (name: string, container: ModuleComponent) => {
            await container.stop();
            this.store.delete(name);
            this.subject.next([name, false]);
        };
        const tasks = [];
        for (const [name, container] of this.store.entries()) {
            tasks.push(remove(name, container));
        }
        await Promise.all(tasks);
        this.state = false;
    }

    protected handler_package(value: any): PackageInfo<ModuleComponent> {
        if(typeof value.name !== "string"){
            const e = new Error(`ох , бл...`);
            log.error('handler_package',e.message,e.stack);
            throw e;
        }
        if (typeof value.default !== "function"
        // || !(value.default instanceof ModuleComponent)
        ) {
            const e = new Error(`ох , бл...`);
            log.error('handler_package',e.message,e.stack);
            throw e;
        }
        let result: ParametersPackageMeta | any = {};
        for (let k of ['name', 'version', 'description', 'dependencies']) {
            if (value.hasOwnProperty(k)) {
                result[k] = value[k];
            }
        }
        return {meta: result, api: value.default};
    }

}
