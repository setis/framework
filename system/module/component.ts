import {Subject, Subscription} from "rxjs";
import {StatusModule} from "../status";
import {CommandModule} from "../command";
import {NameModule} from "../name/component";
import {ModuleModule} from "./module";
import {Notification, TypeLevel, TypeLevelEnum} from "../interface";
import {ConfigModule} from "../config";
import {DependenceModule} from "../dependence";
import {OnInitialization} from "../decorator";
import {ComponentModule} from "../component";

const logger = require('debug')('system:module:component');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}

export class ModuleComponent implements  TypeLevel, OnInitialization {
    initialization: boolean;
    type: TypeLevelEnum = TypeLevelEnum.Module;
    readonly subject: Subject<boolean> = new Subject();
    readonly subscription: Subscription = new Subscription();
    name: NameModule;
    notification: Notification;
    status: StatusModule;
    command: CommandModule;
    config: ConfigModule;
    dependence: DependenceModule;
    api: ComponentModule;
    public constructor(readonly container: ModuleModule, readonly parameters: {
        dependencies?: string[];
        description?: string;
        name: string;
        version?: string;
    }) {

        if (!this.container || !this.parameters || typeof this.parameters.name !== "string") {
            const e = new Error(`не вверные параметры`);
            log.error('constructor', e.message, e.stack)
            throw e
        }
        const {name, version = ''} = this.parameters;
        this.name = this.app.names.use({
            name,
            version,
            type: TypeLevelEnum.Module,
        }) as NameModule;
        log.debug('constructor',name,version,this.name.id);

    }

    get state(): boolean {
        return (this.api !== undefined)
    }

    get app() {
        return this.container.container;
    }

    get components(){
        return this.api.store
    }

    get description(): string {
        return this.parameters.description;
    }

    get dependencies(): string[] {
        return (this.parameters.dependencies) ? this.parameters.dependencies : [];
    }

    async onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription);
        }
        if(this.api){
            await this.api.onDestroy();
            delete this.api;
        }
    }

    async onInit() {
        if (!this.notification) {
            this.notification = (type, call, ...args) => this.app.notification.emit(`system://package.module`, type, call, ...args);
        }
        if (this.subscription) {
            this.subscription
                .add(
                    this.subject.subscribe(value => {
                        switch (value) {
                            case true:
                                this.app.register(this);
                                break;
                            case false:
                                this.app.unregister(this);
                                break;
                            default:
                                throw new Error(`ох , бл...`);
                        }
                    })
                );
            this.app.subscription.add(this.subscription);
        }
        await this.initConfig();
        await this.initCommand();
        await this.initStatus();
        await this.initDependence();
    }

    protected async initDependence() {
        if (!this.dependence) {
            this.dependence = new DependenceModule(this, this.dependencies);
            await this.dependence.onInit();
        }
        await this.dependence.run();
    }

    protected async initConfig() {
        if (!this.config) {
            this.config = new ConfigModule({
                container: this,
                // parent: this.container.config.use(`config.${this.name.name}`)
            });
        }
        await this.config.run();

    }

    protected async initCommand() {
        if (!this.command) {
            this.command = new CommandModule(this);
            await this.command.onInit();
        }
    }

    protected async initStatus() {
        if (!this.status) {
            this.status = new StatusModule(this);
            await this.status.onInit();
        }
    }

    async initInstance(){
        if(!this.api){
            this.api = new ComponentModule({
                container: this
            });
            await this.api.onInit();
        }
    }
    async install(){
        if(!this.api){
            await this.config.run();
            await this.dependence.run();
            await this.initInstance();
            this.subject.next(true);
        }
    }
    async uninstall(){
        if(this.api){
            await this.api.onDestroy();
            delete this.api;
            this.subject.next(false)
        }
    }
    public async start() {
        await this.install();
        await this.api.start();
    }


    public async stop() {
        if (this.api) {
            await this.api.stop();
        }
        await this.uninstall();

    }


}
