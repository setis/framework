export enum TypeEnv {
    PRODUCTION,
    DEVELOPMENT,
    TEST,
}

export function env(): TypeEnv {
    switch (process.env.NODE_ENV) {
        default:
        case "production":
        case "prod":
            return TypeEnv.PRODUCTION;
        case "development":
        case "dev":
            return TypeEnv.DEVELOPMENT;
        case "test":
            return TypeEnv.TEST;
    }
}

export const EnvState = env();

export function isEnvDev(): boolean {
    return EnvState === TypeEnv.DEVELOPMENT;
}

export function isEnvTest(): boolean {
    return EnvState === TypeEnv.TEST;
}

export function isEnvProduction(): boolean {
    return EnvState === TypeEnv.PRODUCTION;
}

export function EnvName(short: boolean = false): string {
    switch (EnvState) {
        case TypeEnv.TEST:
            return "test";
        case TypeEnv.PRODUCTION:
            return (short) ? "prod" : "production";
        case TypeEnv.DEVELOPMENT:
            return (short) ? "dev" : "development";
    }
}

