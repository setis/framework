import {StoreBase} from "./base";
import {ModuleComponent} from "../module";
import {NameModule} from "../name/component";

export class StoreModule extends StoreBase<ModuleComponent,NameModule>{
    filterContainer(value: ModuleComponent): boolean {
        return (value instanceof ModuleComponent);
    }

    filterId(value: NameModule): boolean {
        return (value instanceof NameModule);
    }

}
