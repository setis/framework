import {StoreBase} from "./base";
import {ComponentComponent} from "../component";
import {NameComponent} from "../name/component";

export class StoreComponent extends StoreBase<ComponentComponent,NameComponent>{

    filterContainer(value: ComponentComponent): boolean {
        return value instanceof ComponentComponent;
    }

    filterId(value: NameComponent): boolean {
        return value instanceof NameComponent;
    }

}
