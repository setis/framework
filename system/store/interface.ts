export interface StoreApi {
    store:Map<string,any>;
    register(value:any);
    unregister(value:any);
}
