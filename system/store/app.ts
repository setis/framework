import {StoreBase} from "./base";
import {TypeComponent, TypeLevelEnum} from "../interface";
import {NameComponent, NameModule, TypeName} from "../name";
import {ComponentComponent} from "../component";
import {ModuleComponent} from "../module";

export class StoreApp extends StoreBase<TypeComponent,TypeName>{


    filterContainer(value: TypeComponent): boolean {
        return (value instanceof ComponentComponent || value instanceof ModuleComponent);
    }

    filterId(value: TypeName): boolean {
        return (value instanceof NameComponent || value instanceof NameModule);
    }

    * apps(){
        for (const container of this.store.values()) {
            if(container.type === TypeLevelEnum.App){
                yield container
            }
        }
    }

    /**
     * @description получение списка модулей (ModuleComponent)
     */
    * modules() {
        for (const container of this.store.values()) {
            if(container.type === TypeLevelEnum.Module){
                yield container
            }

        }
    }

    /**
     * @description получение списка компонента (ComponentComponent)
     */
    * components() {
        for (const container of this.store.values()) {
            if(container.type === TypeLevelEnum.Component){
                yield container
            }

        }
    }
}
