import {TypeComponent} from "../interface";
import {TypeName} from "../name";
import {Subject} from "rxjs";
import {StoreApi} from "./interface";

export interface StoreContainer {
    name:TypeName
}
export abstract class StoreBase<Container extends StoreContainer = TypeComponent,Id=TypeName> implements StoreApi,ReadonlyMap<string,Container>{
    forEach(callbackfn: (value: Container, key: string, map: ReadonlyMap<string, Container>) => void, thisArg?: any): void {
        return this.store.forEach(callbackfn,thisArg);
    }
    get(key: string): Container {
        return this.store.get(key)
    }
    has(key: string): boolean {
        return this.store.has(key);
    }
    get size(): number{
        return this.store.size;
    }
    [Symbol.iterator](): IterableIterator<[string, Container]> {
        return this.store[Symbol.iterator]();
    }
    entries(): IterableIterator<[string, Container]> {
        return this.store.entries();
    }
    keys(): IterableIterator<string> {
        return this.store.keys();
    }
    values(): IterableIterator<Container> {
        return this.store.values();
    }
    readonly store:Map<string,Container> = new Map();
    readonly subject:Subject<[string,boolean]|[string,boolean,Container]> = new Subject();
    abstract filterContainer(value:Container):boolean;
    abstract filterId(value:Id):boolean;
    /**
     * @description регистрация в реестре компонент
     * @param value
     */
    register(value: Container) {
        if (!this.filterContainer(value)) {
            throw new Error(` ${value.constructor.name} не является наследником`);
        }
        const id = value.name.id;
        this.store.set(id, value);
        this.subject.next([id, true,value]);
    }

    /**
     * @description удаление из реестра компнента
     * @param value
     */
    unregister(value: Container | string | Id) {
        const remove = (id: string) => {
            if (this.store.has(id)) {
                this.store.delete(id);
                this.subject.next([id,false]);
            }
        };
        switch (typeof value) {
            case "string":
                remove(value);
                break;
            case "object":
                // @ts-ignore
                if (this.filterContainer(value)) {
                    remove((value as Container).name.id);
                } else if (this.filterId(value as Id)) {
                    // @ts-ignore
                    remove((value as Id).id);
                } else {
                    throw new Error(`ох , бл...`)
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }
}
