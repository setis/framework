import {Subject, Subscription} from "rxjs";
import {App} from "../app";
import {OnInitialization} from "../decorator";
import {Notification, TypeComponent, TypeModule} from "../interface";
import {TypeName} from "../name";

const logger = require('debug')('system:mode:base');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};

/**
 * @description контроль режима (работает/не работает) компонента
 */
export abstract class ModeBase implements OnInitialization {
    initialization: boolean = false;
    state: boolean = false;
    readonly config: Map<string, boolean> = new Map();
    default: boolean = true;
    readonly subject: Subject<[TypeName, boolean]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    notification: Notification;
    _subscription?: Subscription;
    abstract container: TypeModule | any;

    abstract app: App;

    onDestroy() {
        if (this.config) {
            this.config.clear();
        }
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription);
        }
    }

    async onInit() {
        if (!this.notification) {
            this.notification = (type, call, ...args) => this.app.notification.emit(type, call, ...args)
        }

        if(this.subscription){
            this.app.subscription.add(this.subscription);
        }
    }

    /**
     * @description автоматическое определение для компонента режима
     * @param value
     */
    async auto(value: TypeComponent) {
        let mode = this.default;
        if (this.config.has(value.name.id)) {
            mode = this.config.get(value.name.id)
        } else if (this.config.has(value.name.name)) {
            mode = this.config.get(value.name.name)
        }
        log.debug(`action: auto mode: ${mode} id: ${value.name.id}`)
        switch (mode) {
            case true:
                await this.enable(value);
                break;
            case false:
                await this.disable(value);
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }
    manual(value: string,mode:boolean){
        log.info(`action: manaul mode: ${mode} id: ${value}`)
        this.config.set(value,mode);
        const container = this.vable(value);
        switch (container.state) {
            case true:
                container.command.start();
                break;
            case false:
                container.command.stop();
                break;
        }
    }
    /**
     * @description запуск модуля
     */
    async start() {
        log.debug(`action: start instance: ${this.constructor.name} state: ${this.state} size: ${this.container.store.size}`)
        if (this.state) {
            return;
        }
        const list = this.container.config.get('mode', {'*': true}) as { [name: string]: boolean };
        for (let name in list) {
            if (name === "*") {
                this.default = list[name];
            } else {
                this.config.set(name, list[name])
            }
        }
        let pool = [];
        for (let [id, component] of this.container.store.entries()) {
            log.debug(`action: start id: ${id} `)
            pool.push(this.auto(component));
        }
        await Promise.all(pool);
        await this._start();
        this.state = true
    }

    /**
     * @description остановка модуля
     */
    async stop() {
        if (!this.state) {
            return;
        }
        log.info('action: stop')
        await this._stop();
        this.state = false;
    }

    async enable(value: string | TypeComponent) {
        let container = this.vable(value)
        log.debug(`mode: enable id: ${container.name.id} class: ${this.constructor.name} use: ${container.constructor.name}`)
        await container.onInit();
        container.command.start();
    }

    async disable(value: string | TypeComponent) {
        let container = this.vable(value)

        log.debug(`mode: disable id: ${container.name.id} class: ${this.constructor.name} use: ${container.constructor.name}`)
        if(container.state){
            container.command.stop();
        }

    }

    /**
     * @description получения компонента
     * @param value
     */
    component(value: string) {
        if (typeof value !== "string") {
            throw new Error(`неверный введенный параметр может только string`);
        }
        if (!this.container.store.has(value)) {
            throw new Error(`не найден компонент: ${value}`);
        }
        return this.container.store.get(value);
    }

    /**
     * @description
     * @private
     */
    protected async _start() {
        for(const [id,container] of this.container.store.entries()){
            if(!container.initialization){
                await container.onInit();
            }
            const mode = container.status.mode(true);
            const next = (!this.config.has(id))?this.default:this.config.get(id);
            if(mode !== next){
                if(next){
                    container.command.start();
                }else{
                    container.command.stop();
                }
            }
        }
        this.resume();
    }

    resume(){
        if (this._subscription) {
            return;
        }
        this._subscription = this
            .container
            .subject
            .subscribe(async value => {
                const [data, state] = value;
                log.info(`action: handler class: ${this.constructor.name} name: ${data.name.id} state: ${state}`)
                if (state === false) {
                    this.subject.next([data.name, false])
                }
                await this.auto(data);
            });
        this.subscription.add(this._subscription);
    }

    /**
     * @description
     * @param value
     */
    abstract is(value: any): boolean;

    /**
     * @description какой сейчас режим или переходит в режим для определеного компонента
     * получет данные из статуса (StatusBase)
     * @param value
     */
    mode(value: string): boolean {
        if (!this.container.store.has(value)) {
            return false;
        }
        const component = this.container.store.get(value);
        return component.status.mode(true) as boolean
    }

    /**
     * @description
     * @private
     */
    protected async _stop() {
        for(let container of this.container.store.values()){
            container.command.stop();
        }
        this.pause();
    }
    pause(){
        if (!this._subscription) {
            return;
        }
        this.subscription.remove(this._subscription);
        this._subscription.unsubscribe();
        delete this._subscription;
    }

    /**
     * @description существует ли это компонент
     * @param value
     */
    has(value: string) {
        if (this.container.store.has(value)) {
            return false;
        }
        const container = this.container.store.get(value);
        return (!container.state);
    }

    /**
     * @description получение списка (ид компнента и его режим)
     */
    * list() {
        for (let [id, component] of this.container.store.entries()) {
            yield [id, component.status.mode(true) as boolean];
        }
    }

    /**
     * @description обработка(передали его ид или сам компонент) и получение компонента
     * @param value
     */
    protected vable(value: string | TypeComponent): TypeComponent {
        if (typeof value === "string") {
            return this.component(value);
        }
        if (!this.is(value)) {
            throw new Error(`неверный введенный параметр`)
        }
        return value;
    }

}
