import {Subject, Subscription} from "rxjs";
import {NameModule} from "../name/component";
import {Boot, OnInitialization} from "../decorator";
import {ModeBase} from "./base";
import {ModuleComponent, ModuleModule} from "../module";
import {Notification} from "../interface";

const logger = require('debug')('system:mode:module');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
@Boot()
export class ModeModule extends ModeBase implements OnInitialization {

    initialization: boolean;
    state: boolean;
    readonly config: Map<string, boolean> = new Map();
    default: boolean = true;
    readonly subject: Subject<[NameModule, boolean]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    notification: Notification;

    constructor(readonly container: ModuleModule) {
        super();
    }

    get app() {
        return this.container.container;
    }


    is(value: any): boolean {
        return (value instanceof ModuleComponent);
    }

}
