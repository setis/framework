import {Boot, OnInitialization} from "../decorator";
import {Subject, Subscription} from "rxjs";
import {NameComponent} from "../name/component";
import {ModeBase} from "./base";
import {App} from "../app";
import {ComponentComponent, ComponentModule} from "../component";
import {Notification} from "../interface";

const logger = require('debug')('system:mode:component');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
@Boot()
export class ModeComponent extends ModeBase implements OnInitialization{
    initialization: boolean;
    state: boolean;
    readonly config: Map<string, boolean> = new Map();
    default: boolean = true;
    readonly subject: Subject<[NameComponent, boolean]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    notification: Notification;

    constructor(readonly container: ComponentModule) {
        super();
    }

    get app(): App {
        return this.container.app;
    }

    is(value: any): boolean {
        return (value instanceof ComponentComponent);
    }

}
