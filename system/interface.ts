import {ConfigProperty} from "../elements/configure";
import {LoaderOnceConfigure} from "../bases/loader";
import {EventEmitter} from "events";
import {PathComponent} from "../elements/path";
import {App} from "./app";
import {ComponentComponent, ComponentModule} from "./component";
import {ModuleComponent, ModuleModule} from "./module";

export interface ParametersPackageInfo<T> {
    default: T;
    dependencies?: string[];
    description?: string;
    name: string;
    version?: string;
}
export interface ParametersPackageMeta {
    dependencies?: string[];
    description?: string;
    name: string;
    version?: string;
}
export interface PackageInfo<T>{
    meta:ParametersPackageMeta,
    api:T
}
export interface ParametersPackageComponent<T> {
    config?: ConfigProperty<LoaderOnceConfigure> | string;
    notification?: EventEmitter | Notification;
    container: T
}

export interface ParametersPackageModule<T> extends ParametersPackageComponent<T> {
    path?: PathComponent;
    app: App
}

export type TypeComponent = ComponentComponent | ModuleComponent;
export type TypeModule = ComponentModule | ModuleModule;

export type Notification = (type: string, call: string, ...args: any) => void;

export interface OnInit {
    onInit();
}

export interface OnDestroy {
    onDestroy();
}

export interface OnUp {
    up(value: string): Promise<string>;
}

export interface OnDown {
    down(value: string): Promise<void>;
}

export enum TypeLevelEnum {
    App,
    Module,
    Component
}

export interface TypeLevel {
    readonly type: TypeLevelEnum
}

export interface Module {
    [x: string]: any;
    api: ModuleComponent;
    start();
    stop();
}

export interface Component {
    [x: string]: any;
    api: ComponentComponent;
    start();
    stop();
}
