import {NameComponent} from "../name/component";
import {Subject, Subscription} from "rxjs";
import {ComponentModule} from "./module";
import {StatusComponent} from "../status";
import {CommandComponent} from "../command";
import {ParametersPackageInfo, TypeLevel, TypeLevelEnum} from "../interface";
import {Component, ConfigComponent, DependenceComponent} from "../index";

const logger = require('debug')('system:component:component');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
}
export class ComponentComponent implements TypeLevel {
    readonly type: TypeLevelEnum = TypeLevelEnum.Component;
    public instance?: Component;
    public name: NameComponent;
    readonly subject: Subject<boolean> = new Subject();
    readonly subscription: Subscription = new Subscription();
    config: ConfigComponent;
    status: StatusComponent;
    dependence:DependenceComponent;
    command: CommandComponent;
    public constructor(readonly parameters: {
        container: ComponentModule,
        package: ParametersPackageInfo<Component>,
    }) {
        if(!this.container || !this.parameters || typeof this.package.name !== "string"|| typeof this.package.default !== "function"){
            const e = new Error(`не вверные параметры`);
            log.error(e.message, e.stack)
            throw e
        }
        const {name, version} = this.package;

        this.name = this.app.names.use({
            module: this.container.name.id,
            name,
            version,
            type: TypeLevelEnum.Component,
        }) as NameComponent;
        log.debug('constructor', name, version, this.name.id,this.name.constructor.name);
    }

    get api(){
        if(this.instance === undefined){
            throw new Error(`ох , бл...`);
        }
        return this.instance;
    }

    get state(): boolean {
        return (this.instance !== undefined)
    }

    get package() {
        return this.parameters.package;
    }

    get container() {
        return this.parameters.container;
    }

    public get loader() {
        if (typeof this.package.default !== "function") {
            throw new Error(`ох , бл...`);
        }

        return this.package.default;
    }

    get description(): string {
        return this.package.description;
    }

    get dependencies(): string[] {
        return (this.package.dependencies) ? this.package.dependencies : [];
    }

    get app() {
        return this.container.app;
    }

    get module() {
        return this.container.container;
    }

    async onDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription);
        }
        await this.app.unregister(this);
    }

    async onInit() {
        if (this.subscription) {
            this.subscription
                .add(
                    this.subject.subscribe(value => {
                        switch (value) {
                            case true:
                                this.app.register(this);
                                break;
                            case false:
                                this.app.unregister(this);
                                break;
                            default:
                                throw new Error(`ох , бл...`);
                        }
                    })
                )
            this.app.subscription.add(this.subscription);
        }
        await this.initCommand();
        await this.initStatus();
        await this.initConfig();
        await this.initDependence()

    }
    protected async initCommand(){
        if(!this.command){
            this.command = new CommandComponent(this);
            await this.command.onInit();
        }
    }
    protected async initStatus(){
        if(!this.status){
            this.status = new StatusComponent(this);
            await this.status.onInit();
        }
    }
    protected async initDependence() {
        if (!this.dependence) {
            this.dependence = new DependenceComponent(this, this.dependencies);
            await this.dependence.onInit();
        }
        await this.dependence.run();
    }
    protected async initConfig() {
        if (!this.config) {
            this.config = new ConfigComponent({
                container: this
            });
            await this.config.run();
        }
    }

    public async start() {
        if (!this.instance) {
            try{
                this.instance = new (this.loader as any)(this);
                if (typeof this.instance.onInit === "function") {
                    await this.instance.onInit();
                }
            }catch (e) {
                log.error(e.message)
                throw e;
            }
        }
        this.subject.next(true)

    }

    public async stop() {
        if (this.instance) {
            if (typeof this.instance.onDestroy === "function") {
                await this.instance.onDestroy();
            }
        }
        delete this.instance;
        this.subject.next(false)
    }
}
