import {Boot, OnInitialization} from "../decorator";
import {LoaderOnce, LoaderOnceConfigure} from "../../bases/loader";
import {Subject, Subscription} from "rxjs";
import {ComponentComponent} from "./component";
import {ModeComponent} from "../mode";
import {NameComponent, NameModule} from "../name/component";
import {App} from "../app";
import {PathComponent} from "../../elements/path";
import {ConfigProperty} from "../../elements/configure";
import {EventEmitter} from "events";
import {RegistryOnce} from "../../elements/registry";
import {ParametersNameComponent, ParametersNameModule} from "../name";
import {Component, ModuleComponent, StatusEnum} from "../index";
import {Notification, ParametersPackageInfo, TypeLevelEnum} from "../interface";

const logger = require('debug')('system:component:module');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug')
};
@Boot()
export class ComponentModule extends LoaderOnce implements OnInitialization {
    initialization: boolean;
    readonly subject: Subject<[ComponentComponent, boolean]> = new Subject();
    public container: ModuleComponent;
    readonly store: Map<string, ComponentComponent> = new Map();
    readonly subscription: Subscription = new Subscription();
    mode: ModeComponent;

    public constructor(readonly parameters: {
        config?: ConfigProperty<LoaderOnceConfigure> | string;
        notification?: EventEmitter | Notification;
        container: ModuleComponent
        path?: PathComponent;
    }) {
        super();
    }
    state: boolean;

    get name(): NameModule {
        return this.container.name;
    }

    get app():App {
        return this.container.container.container;
    }

    public [Symbol.iterator]() {
        return this.store[Symbol.iterator]();
    }

    public values() {
        return this.store.values();
    }

    public entries() {
        return this.store.entries();
    }

    public has(name: string) {
        return this.store.has(name);
    }

    public keys() {
        return this.store.keys();
    }

    public async handler(data: ParametersPackageInfo<Component>, path: string): Promise<string> {
        // log.debug(`action: handler path: ${path} component: ${data.default.constructor.name}`)
        let container;
        try{
            container = this.instance(data);
        }catch (e) {
            log.error(`path: ${path} msg: ${e.message}`);
            throw e;
        }
        this.register(container);
        return container.name.id;

    }
    instance(value:ParametersPackageInfo<Component>){
        return new ComponentComponent({
            container: this,
            package:  this.handler_package(value),
        });
    }
    async onDestroy() {
        await this.stop();
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.app.subscription.remove(this.subscription)
        }
    }

    async onInit() {
        const {config, notification, container, path} = this.parameters;

        if (!this.container) {
            if (container.type !== TypeLevelEnum.Module) {
                throw new Error(`not found parameter:container`);
            }
            this.container = container;
        }
        if (!this.path) {
            if (path) {
                switch (typeof path) {
                    case "string":
                        this.path = new PathComponent(path);
                        break;
                    case "object":
                        if (path instanceof PathComponent) {
                            this.path = path;
                        }
                        break;
                    default:
                        throw new Error(`ох , бл...`);
                }
            }
            if (!this.path) {
                this.path = new PathComponent(`${this.app.path}/${this.container.name}s`)
            }
        }
        if (!this.config) {
            if (config instanceof ConfigProperty) {
                this.config = config;
            }
            else {
                // this.config = this.container.config.use('loader')
                this.config = this.app.config.use(`config.${this.container.name}`)
            }
        }
        if (!this.notification) {
            const id = `app://${this.container.name}.package`;

            if (typeof notification === "function") {
                this.notification = notification;
            } else if (notification instanceof EventEmitter) {
                this.notification = (type, call, ...args) => notification.emit(type, `${id}#${call}`, ...args);
            } else {
                this.notification = (type, call, ...args) => this.app.notification.emit(type, `${id}#${call}`, ...args);
            }

        }
        if (!this.registry) {
            this.registry = new RegistryOnce();
            this.registry.root = this.path.root;
        }
        this.path.load(this.config.get("loader.path", []));
        this.registry.load(this.config.get("loader.registry", {}));
        this.package = this.config.get("loader.package", []);
        if (this.subscription) {
            this.subscription.add(
                this.subject.subscribe(value => {
                    let [component, state] = value;
                    this.app.status.next([component.name.id, (state) ? StatusEnum.Init : StatusEnum.Destroy])
                })
            );
            this.app.subscription.add(this.subscription);
        }
        await this.initMode();

    }
    async initMode(){
        if(!this.mode){
            this.mode = new ModeComponent(this);
            await this.mode.onInit();
        }
    }

    public register(value: ComponentComponent) {
        if (!(value instanceof ComponentComponent)) {
            const error = new Error(`не является ComponentComponent`);
            this.notification("error", "register", error);
            throw error;
        }
        this.store.set(value.name.id, value);
        this.subject.next([value, true]);
    }

    public async unregister(value: ComponentComponent | string | NameComponent | ParametersNameComponent) {
        const remove = async (value: string) => {
            const container = this.store.get(value);
            await container.start();
            this.store.delete(value);
            this.subject.next([container, true]);
        };
        switch (typeof value) {
            case "string":
                if (this.store.has(value)) {
                    await remove(value);
                }
                break;
            case "object":
                if (value instanceof NameModule) {
                    await remove(value.id);
                } else if (value instanceof ComponentComponent) {
                    await remove(value.name.id);
                } else {
                    const name = new NameModule(value as ParametersNameModule);
                    await remove(name.id);
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    protected handler_package(value: any): ParametersPackageInfo<Component> {
        let result: ParametersPackageInfo<Component> | any = {};
        for (let k of ['name', 'version', 'description', 'default', 'dependencies']) {
            if (value.hasOwnProperty(k)) {
                result[k] = value[k];
            }
        }
        if (typeof result.name !== 'string') {
            throw new Error(`не обзначено имя для пакета`);
        }
        if (typeof result.default !== "function") {
            throw new Error(`в пакете ${result.name} запускной не является функцикй или классом`);
        }
        return result;
    }

    async start(){
        if(this.state){
            return;
        }
        await this.run();
        await this.mode.start();
        this.state = true;
    }

    async stop(){
        if(!this.state){
            return;
        }
        await this.mode.stop();
        const remove = async (name: string, container: ComponentComponent) => {
            await container.stop();
            this.store.delete(name);
            this.subject.next([container, true]);
        };
        const tasks = [];
        for (const [name, container] of this.store.entries()) {
            tasks.push(remove(name, container));
        }
        await Promise.all(tasks);
        this.state = false;
    }
}
