import {EventEmitter} from "events";
import {Subject, Subscription} from "rxjs";
import {Name, NameComponent, NameModule, TypeName} from "./name";
import {ConfigFile, ConfigProperty, ConfigSetting} from "../elements/configure";
import {ConfigApp} from "./config";

import {OnInitialization} from "./decorator";
import {ModuleComponent, ModuleModule} from "./module";
import {ComponentComponent} from "./component";
import {TypeComponent, TypeLevel, TypeLevelEnum} from "./interface";
import {UpDown} from "./updown";
import {ConfigParametes} from "./config/base";

const sort = require('version-sort');
export interface ParametersApp<Configure = any> {
    config?: ConfigSetting | ConfigFile | ConfigProperty | Configure;
    name?: string;
    notification?: EventEmitter;
    path?: string;
}
const logger = require('debug')('app');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    debug: logger.extend('debug'),
    log: logger.extend('log')
};

export class App<Configure = any> implements TypeLevel, OnInitialization {
    state: boolean = false;
    initialization: boolean;
    readonly type: TypeLevelEnum = TypeLevelEnum.App;
    readonly names: Name = new Name();
    updown:UpDown = new UpDown(this);
    path: string;
    notification: EventEmitter;
    readonly status: Subject<[string, string]> = new Subject();
    readonly command: Subject<[string, string]> = new Subject();
    readonly dependence: Subject<[string, boolean]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    config: ConfigApp<App, Configure>;
    packages: ModuleModule;
    readonly binds:Map<string,string> = new Map();
    readonly store: Map<string, TypeComponent> = new Map();
    readonly subject: Subject<[TypeComponent, boolean]> = new Subject();
    constructor(readonly parameters: { config?: any; notification?: EventEmitter; path?: string; name?: string }) {

    }

    get name() {
        return (this.parameters.name) ? this.parameters.name : ""
    }

    public async onDestroy() {
        await Promise
            .all(
                Object
                    .keys(this)
                    .filter((value) => this[value].hasOwnProperty("onDestroy"))
                    .map(async (value) => (value as any).onDestroy()),
            );
        this.destroySubscription();
    }

    /**
     * @description проверка является ли компнентом,
     * осуществалается проверка наследование ComponentComponent, ModuleComponent
     * @param value
     */
    static is(value: TypeComponent): boolean {
        return (value instanceof ComponentComponent || value instanceof ModuleComponent);
    }

    async onInit() {
        this.initPath();
        this.initNotification();
        await this.initConfig();
        await this.initPackage();
        this.initSubscription();
    }


    protected destroySubscription(){
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    protected initPath(){
        if(!this.path){
            const {path} = this.parameters;
            if (typeof path === "string") {
                this.path = path;
            } else if (!this.path) {
                this.path = process.cwd();
            }
        }
    }
    protected initNotification(){
        if(!this.notification){
            const {notification} = this.parameters;
            if (notification && notification instanceof EventEmitter) {
                this.notification = notification;
            } else if (!this.notification) {
                this.notification = new EventEmitter();
                this.notification.setMaxListeners(0);
            }
        }
    }
    protected initSubscription(){
        if(this.subscription){
            this.subscription
                .add(
                    this.status.subscribe(value => {
                        const [name, status] = value;
                        log.info(`status: ${status} name: ${name} `)
                    })
                )
                .add(
                    this.dependence.subscribe(value => {
                        const [name, state] = value;
                        log.info( `dependence: ${state} name: ${name} `)
                    })
                )
                .add(
                    this.command.subscribe(value => {
                        const [name, command] = value;
                        log.info( `command: ${command} name: ${name} `)
                    })
                )

        }
    }
    protected async initConfig(){
        if (!this.config) {
        const { config} = this.parameters;
        let parameters: ConfigParametes = {container: this};
        // config
        if (config) {

            if (config instanceof ConfigFile) {
                parameters.file = config;
            } else if (config instanceof ConfigProperty) {
                parameters.data = config;
            } else if ((["path", "pattern"].map((value) => config.hasOwnProperty(value)).length === 2)) {
                parameters.file = config as ConfigSetting;
            } else {
                parameters.data = config as Configure;
            }

        }
            this.config = new ConfigApp(parameters);
            await this.config.run();
        }
    }
    protected async initPackage(){
        if (!this.packages) {
            this.packages = new ModuleModule({
                container: this,
                config: this.config.config as ConfigProperty
            })
            await this.packages.onInit();
            await this.config.run();
        }
    }
    /**
     * @description запуск приложения
     */
    async start() {
        log.info('action: start')
        if (this.state) {
            return;
        }
        await this.config.run();
        await this.packages.start();
        this.state = true;
    }

    /**
     * @description остановка приложения
     */
    async stop() {
        log.info('action: stop');
        if (!this.state) {
            return;
        }
        await this.packages.stop();
        this.state = false;
    }

    /**
     * @description регистрация в реестре компонент
     * @param value
     */
    register(value: TypeComponent) {
        if (!App.is(value)) {
            throw new Error(` ${value.constructor.name} не является наследником ComponentComponent или ModuleComponent`);
        }
        const id = value.name.id;
        this.store.set(id, value);
        // this.subject.next([value, true]);
        // this.status.next([id,  StatusEnum.Init]);

    }

    /**
     * @description удаление из реестра компнента
     * @param value
     */
    async unregister(value: TypeComponent | string | TypeName) {
        // const remove = async (id: string) => {
        //     if (this.store.has(id)) {
        //         const container = this.store.get(id);
        //         if (typeof container.onDestroy === "function") {
        //             await container.onDestroy();
        //         }
        //         this.store.delete(id);
        //         this.subject.next([container, false])
        //         this.status.next([id,  StatusEnum.Destroy]);
        //     }
        // };
        const remove = async (id: string) => {
            if (this.store.has(id)) {
                this.store.delete(id);
            }
        };
        switch (typeof value) {
            case "string":
                await remove(value);
                break;
            case "object":
                if (App.is(value as TypeComponent)) {
                    await remove((value as TypeComponent).name.id);
                } else if (Name.is(value as TypeName)) {
                    await remove((value as TypeName).id);
                } else {
                    throw new Error(`ох , бл...`)
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    /**
     * @description получение компонента (ModuleComponent|ComponentComponent),
     * если не указана версия component://typeorm найдет любую версию,
     * если из найденых несколько версия выбирит без версионную или последную версию
     * @param value
     * @param strict точность версии например: component://typeorm@0.3.0-alpha.24
     */
    use(value: string|TypeName,strict:boolean = false): TypeComponent {
        const read = (value: string) => {
            if(strict){
                if (!this.store.has(value)) {
                    log.debug(`action: use store: ${Array.from(this.store.keys()).join(', ')}`);
                    throw new Error(`не найден ${value}`);
                }
                return this.store.get(value)
            }else{
                if(this.store.has(value)){
                    return this.store.get(value);
                }
                if(this.binds.has(value)){
                    return this.use(this.binds.get(value),true)
                }
                const name = this.names.use(value,true);
                const list = Array.from(this.store.values())
                    .filter(value1 => {
                        switch (name.type) {
                            case TypeLevelEnum.Component:
                                return value1.type === name.type && (value1.name as NameComponent).module === (name as NameComponent).module;
                            case TypeLevelEnum.Module:
                                return value1.type === name.type && (value1.name as NameModule).name === (name as NameModule).name;
                        }
                    });
                if(list.length  === 1){
                    const container =  list[0];
                    this.binds.set(name.id,container.name.id);
                    return container;
                }
                if(list.length === 0 ){
                    log.debug(`action: use store: ${Array.from(this.store.keys()).join(', ')}`);
                    throw new Error(`не найден ${value}`);
                }
                const ms = list.filter(value1 => value1.name.version === undefined);
                if(ms.length> 0){
                    this.binds.set(name.id,ms[0].name.id);
                    return ms[0];
                }
                const versions = sort(
                    list
                        .map(value1 => value1.name.version)
                )
                    .reverse()
                    .filter(value1 => name.check(value1))
                if(versions.length === 0){
                    log.debug(`action: use store: ${Array.from(this.store.keys()).join(', ')}`);
                    throw new Error(`не найден ${value}`);
                }
                const id = name.id;
                name.version = versions[0];
                this.binds.set(id,name.id);
                return this.store.get(name.id);
            }
        };
        if (typeof value === "string") {
            return read(value);
        } else if (typeof value === "object" && Name.is(value)) {
            return read(value.id);
        } else {
            throw new Error(`ох , бл...`);
        }
    }

    /**
     * @description получение списка модулей (ModuleComponent)
     */
    * modules() {
        for (let module of this.packages.values()) {
            yield module
        }
    }

    /**
     * @description получение списка компонента (ComponentComponent)
     */
    * components() {
        for (let module of this.packages.values()) {
            for (let component of module.components.values()) {
                yield component
            }
        }
    }


}
