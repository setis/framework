import {DependenceModule} from "./module";
import {DependenceComponent} from "./component";

export * from './module'
export * from './component'
export type TypeDependence = DependenceModule | DependenceComponent;
