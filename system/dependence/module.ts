import {OnInitialization, Run} from "../decorator";
import {ModuleComponent, TypeLevel, TypeLevelEnum} from "../index";
import {DependenceBase} from "./base";

@Run()
export class DependenceModule extends DependenceBase<ModuleComponent> implements OnInitialization {
    initialization: boolean;

    get name() {
        return this.container.name;
    }

    get status() {
        return this.container.status;
    }

    get store() {
        return this.app.store;
    }

    get app() {
        return this.container.app;
    }

    async run() {
        this.check();
    }

    protected filter(value: TypeLevel): boolean {
        return value.type === TypeLevelEnum.Module;
    }


}
