import {OnInitialization, Run} from "../decorator";
import {ComponentComponent, TypeLevel, TypeLevelEnum} from "../index";
import {DependenceBase} from "./base";

@Run()
export class DependenceComponent extends DependenceBase<ComponentComponent> implements OnInitialization{
    initialization: boolean;

    async run() {
        this.check();
    }
    get name() {
        return this.container.name;
    }

    get status() {
        return this.container.status;
    }
    get app(){
        return this.container.app;
    }
    get store() {
        return this.app.store;
    }

    protected filter(value: TypeLevel): boolean {
        return value.type === TypeLevelEnum.Component;
    }

}
