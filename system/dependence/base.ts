import {Observable as ObjectObservable} from "object-observer/dist/node/object-observer.js";
import {ObserverBase} from "object-observer";
import {Subject, Subscription} from "rxjs";
import {distinct, filter} from "rxjs/operators";
import {App} from "../app";
import {StatusBase} from "../status";
import {TypeName} from "../name";
import {OnDestroy, OnInit, TypeComponent} from "../interface";
import {ModuleComponent} from "../module";
import {ComponentComponent} from "../component";

/**
 * @description
 */
export abstract class DependenceBase<Container = TypeComponent> implements OnInit,OnDestroy{
    readonly subject: Subject<boolean> = new Subject();
    abstract app: App;
    abstract status: StatusBase;
    abstract name: TypeName;
    readonly change: Subject<{ type: string, name: string }[]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    readonly notification:Subject<[string,boolean]> = new Subject();
    set state(value: boolean) {
        if (typeof value !== "boolean") {
            throw new Error(`ох , бл...`);
        }
        this._state = value;
        this.subject.next(this._state);
    }

    constructor(readonly container: Container, dependencies?: string[]) {

        if (dependencies) {
            this.dependencies = [...dependencies];
        }
    }

    protected _state: boolean = false;

    get state(): boolean {
        return this._state;
    }


    private _dependencies: string[] & ObserverBase<string[]>;

    get dependencies(): string[] {
        return this._dependencies;
    }

    set dependencies(value: string[]) {
        if (this._dependencies !== value) {
            if (ObjectObservable.isObservable(this._dependencies)) {
                this._dependencies.unobserve();
            }
        }
        if (!ObjectObservable.isObservable(value)) {
            this._dependencies = ObjectObservable.from(value);
            this._dependencies.observe((changes) => {
                this.change.next(
                    changes.map((value1) => {
                        return {type: value1.type, name: value1.value};
                    }),
                );
                this.check();
            });
        }

    }
    onInit(){
        if(this.subscription){
            this.subscription
                .add(
                    this.subject.subscribe(value => {
                        this.notifications(value);
                        // @ts-ignore
                        this.app.dependence.next([this.container.name.id, value])
                    })
                )
                .add(
                    this.notification.subscribe(value => {
                        // const [name,state] = value;
                        this.check()
                    })
                );
            ;
            this.app.subscription.add(this.subscription);
        }
    }
    onDestroy(){
        if(this.subscription){
            this.app.subscription.remove(this.subscription);
            this.subscription.unsubscribe();
        }
    }



    /**
     * @description когда станет все зависимости запущеные
     */
    public async up() {
        if (this.state === true) {
            return;
        }
        await this
            .subject
            .pipe(
                distinct(),
                filter((value) => value === true),
            )
            .toPromise();
    }
    /**
     * @description когда станет ожидить запуска зависимостей
     */
    public async down() {
        if (this.state === false) {
            return;
        }
        await this
            .subject
            .pipe(
                distinct(),
                filter((value) => value === false),
            )
            .toPromise();
    }
    /**
     * @todo добавить версионность
     * @description получить список зависимых (компненты у которых есть зависимостях это компонент)
     */
    public dependents(): (ComponentComponent | ModuleComponent)[] {
        return Array
            .from(this.app.store.values())
            .filter((value) => {
                if (!value.hasOwnProperty("dependencies") || !(value.dependencies instanceof Array && value.dependencies.length > 0)) {
                    return false;
                }
                if (value.dependencies.includes(this.name.id)) {
                    return true;
                }
                return value
                    .dependencies
                    .map((value1) => this.app.names.use(value1))
                    // .filter((value1) => value1.id === this.name.id || this.filter(value1))
                    .length > 0;
            });
    }

    /**
     * @todo добавить версионность
     * @description проверка на зависимости
     */
    check(){
        for(const name of this.dependencies){
            try{
                const container = this.app.use(name);
                if (container.status.get() !== container.status.online) {
                    return this.state = false;
                }
            }catch (e) {
                return this.state = false;
            }
        }
        return this.state = true;
    }

    /**
     * @description уведомлять зависимые о состоянии
     * @param state
     */
    notifications(state:boolean){
        for(const component of this.dependents()){
            try{
                const container = this.app.use(component.name);
                container.dependence.notification.next([this.name.id,state])
            }catch (e) {
            }
        }
    }
    /**
     * @description проверка на всех запущеных зависимости/остановленых зависимых компонетов
     * @param type
     */
    public depends(type: string|boolean): boolean {
        switch (type) {
            case true:
            case this.status.online:
                for (const name of this.dependencies) {
                    try {
                        const container = this.app.use(name);
                        if (container.status.get() !== type) {
                            return this.state = false;
                        }
                    } catch (e) {
                        return this.state = false;
                    }
                }
                return this.state = true;
            case false:
            case this.status.offline:
                for (const component of this.dependents()) {
                    try {
                        if (component.status.get() !== type) {
                            return this.state = false;
                        }
                    } catch (e) {
                        return this.state = false;
                    }
                }
                return this.state = true;
            default:
                throw new Error(`ох , бл...`);
        }

    }

    /**
     * @description
     * @example (value1.type === "component" && value1.name === this.name && value1.check(this.name.version))
     * @param value
     */
    protected abstract filter(value: Container): boolean;
}
