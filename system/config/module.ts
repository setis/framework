import {ConfigFile, ConfigProperty} from "../../elements/configure";
import {ModuleComponent} from "../module";
import {ConfigBase, ConfigOptions, ConfigPath, TypeConfig} from "./base";

export interface ConfigModuleOptions {
    app: ConfigPath<ConfigModule>;
    module: ConfigOptions<ConfigModule>;
}
const logger = require('debug')('system:config:module');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    debug: logger.extend('debug'),
    log: logger.extend('log')
}
export class ConfigModule<Data = any> extends ConfigBase<ModuleComponent, Data, ConfigModuleOptions> {

    protected _options_default = {
        app(this:ConfigModule) {
            return `config.${this.container.name.name}`
        },
        module(this:ConfigModule) {
            return {
                path: `${this.app.path}/resource/config`,
                pattern: [
                    `{name}.{ext}`,
                    `{name}-{env}.{ext}`,
                    `{name}/{env}.{ext}`,
                    `{app}/{name}.{ext}`,
                    `{app}/{name}-{env}.{ext}`,
                    `{app}/{name}/{env}.{ext}`,
                    `{app}-{name}-{env}.{ext}`,
                ],
                params: {
                    name: this.container.name.name,
                    app: this.app.name,
                },
                env: true,
            };
        },
    };

    public get app() {
        return this.container.container.container;
    }

    public async run() {
        if(this.config instanceof ConfigFile){
            await this.config.run();
            this.type = TypeConfig.File;
            return;
        }
        else if(this.config instanceof ConfigProperty){
            this.type = TypeConfig.Data;
            return;
        }
        try {
            let path = this._path(this.options.app);
            if (this.app.config.get(path, false)) {
                this.type = TypeConfig.Parent;
                this.config = this.app.config.use(path);
                log.info(`id: ${this.container.name.id} parent: app path: ${path} full: ${this.config.full()}`)
                return;
            }
        } catch (e) {
            log.info(`id: ${this.container.name.id} error: ${e.message}`)
        }
        const container = new ConfigFile<Data>(this._config(this.options.module));
        await container.run();
        this.type = TypeConfig.File;
        this.config = container;
    }
}
