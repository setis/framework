import {ConfigFile, ConfigProperty} from "../../elements/configure";
import {ConfigBase, ConfigOptions, TypeConfig} from "./base";

export class ConfigApp<Container = any, Data = any> extends ConfigBase<Container, Data, ConfigOptions<ConfigApp>> {

    public async run() {
        if(this.config instanceof ConfigFile){
            return await this.config.run();
        }
        else if(this.config instanceof ConfigProperty){
            return
        }
        else{
            const container = new ConfigFile<Data>(this._config(this.options));
            await container.run();
            this.config = container;
            this.type = TypeConfig.File;
        }

    }

    protected _options_default = function () {
        return {
            path: `${this.container.path}/resource/config`,
            pattern: [
                `{name}/{env}.{ext}`,
                `{name}-{env}.{ext}`,
                `{name}.{ext}`,
                `app/{name}/{env}.{ext}`,
                `app/{name}-{env}.{ext}`,
                `app/{name}.{ext}`,
            ],
            params: {
                name: this.container.name,
            },
            env: false,
        };
    };

}
