import {ConfigFile, ConfigProperty, ConfigSetting} from "../../elements/configure";
import Timeout = NodeJS.Timeout;

export interface ConfigParametes<Container = any, Data = any, Options = any> {
    container: Container
    options?: Options,
    cache?: {
        mode?: boolean,
        ttl?: number
    }
    data?: Data | ConfigProperty<Data>,
    file?: ConfigFile<Data> | ConfigSetting
    parent?: {
        store: ConfigProperty<Data>,
        path: string
    } | ConfigProperty
}
export enum TypeConfig {
    Parent,
    File,
    Data
}

export type ConfigOptions<Self = ConfigBase> = ConfigSetting | ((this: Self) => ConfigSetting) ;
export type ConfigPath<Self = ConfigBase> = string | ((this: Self) => string) ;
export abstract class ConfigBase<Container = any, Data = any, Options = any> {
    public type: TypeConfig;
    public config: ConfigProperty<Data> | ConfigFile<Data>;
    cache_mode: boolean = false;
    cache_ttl: number = 60e3 * 5;
    readonly cache_ttls: Map<string, Timeout> = new Map();
    readonly cache_store: Map<string, ConfigProperty<any>> = new Map();
    protected _options_default: Options;
    protected default: Data;
    container: Container;
    public constructor(parameters: ConfigParametes<Container, Data, Options>) {
        let {container, options, data, cache, file, parent} = parameters;
        if (cache) {
            const {mode = this.cache_mode, ttl = this.cache_ttl} = cache;
            this.cache_mode = mode;
            this.cache_ttl = this.cache_ttl;
        }

        if (options) {
            this.options = options;
        }

        if (data) {
            if (data instanceof ConfigProperty) {
                this.config = data
            } else {
                this.config = new ConfigProperty();
                this.config.data = data;
            }
            this.type = TypeConfig.Data;
        } else if (file) {
            if (file instanceof ConfigFile) {
                this.config = file;
            } else {
                this.config = new ConfigFile(file as ConfigSetting);
            }
            this.type = TypeConfig.File;
        } else if (parent) {
            if (parent instanceof ConfigProperty) {
                this.config = parent;
            } else if (typeof parent.path === "string" && parent.store instanceof ConfigProperty) {
                const {store, path} = parent;
                this.config = store.use(path);
            }
            this.type = TypeConfig.Parent;
        }
        if (container) {
            this.container = container;
        }

    }


    protected _options: Options | any;

    public get options() {
        return (this._options) ? this._options : this._options_default;
    }

    public set options(value: Options | any) {
        if (typeof this._options_default === "object") {
            this._options = {
                ...this._options_default,
                ...value,
            };
        } else {
            this._options = value;
        }

    }

    public get(): Data;

    public get<T = any>(name: string): T;

    public get<T = any>(name: string, defaults: T): T;

    public get<T = any>(name?: string, defaults?: T): T {
        return (arguments.length === 0) ? this.config.data : this.config.get(name, defaults);
    }

    get subject() {
        return this.config.subject;
    }

    public set(value: Data);

    public set(path: string, value: any);

    public set() {
        if (arguments.length === 1) {
            this.config.data = arguments[0];
        } else {
            this.config.set(arguments[0], arguments[1]);
        }
    }

    public use(): ConfigProperty<Data>;
    public use(path: string): ConfigProperty<Data>;
    public use(path?: string): ConfigProperty<any> {
        if (!path) {
            return this.config;
        }

        if (this.cache_mode && this.cache_store.has(path)) {
            return this.cache_store.get(path);
        }
        const data = this.config.use(path);
        if (this.cache_mode) {
            this.cache_store.set(path, data);
            this.cache_ttls.set(path, setTimeout(() => this.cache_store.delete(path), this.cache_ttl));
        }
        return data;
    }

    protected _config(value: ((this: this) => ConfigSetting) | ConfigSetting): ConfigSetting {
        switch (typeof value) {
            case "function":
                return value.call(this, this);
            case "object":
                if (typeof value.path === "string"
                    && typeof value.pattern === "object"
                    && value.pattern instanceof Array
                    && value.pattern.filter((value1) => typeof value1 !== "string").length == 0
                ) {
                    return value;
                }
                throw new Error(` данные не явлется типом ConfigSetting ${JSON.stringify(value)}`);
            default:
                throw new Error(`тип может быть только object или function, но не ${typeof value}`);

        }
    }

    protected _path(value: ((this: this) => string) | string): string {
        switch (typeof value) {
            case "function":
                return value.call(this, this);
            case "string":
                return value;
            default:
                throw new Error(`ох , бл...`);
        }
    }
}
