import {ConfigFile, ConfigProperty} from "../../elements/configure";
import {ComponentComponent} from "../component";
import {ConfigBase, ConfigOptions, ConfigPath, TypeConfig} from "./base";

export interface ConfigComponentOptions {
    app?: ConfigPath<ConfigComponent>;
    component?: ConfigOptions<ConfigComponent>;
    module?: ConfigPath<ConfigComponent>;
}

const logger = require('debug')('system:config:component');
const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    debug: logger.extend('debug'),
    log: logger.extend('log')
}
export class ConfigComponent< Data = any> extends ConfigBase<ComponentComponent, Data, ConfigComponentOptions> {
    public type: TypeConfig;

    // @ts-ignore
    protected _options_default = {
        app(this:ConfigComponent) {
            return  `config.${this.module.name.name}.config.${this.container.name.name}`
        },
        module(this:ConfigComponent) {
            return `config.${this.container.name.name}`;
        },
        component(this:ConfigComponent) {
            return {
                path: `${this.app.path}/resource/config`,
                pattern: [
                    `{module}/{name}.{ext}`,
                    `{module}/{name}-{env}.{ext}`,
                    `{module}-{name}.{ext}`,
                    `{module}-{name}-{env}.{ext}`,
                    `{app}/{module}/{name}.{ext}`,
                    `{app}/{module}/{name}-{env}.{ext}`,
                    `{app}/{module}-{name}.{ext}`,
                    `{app}/{module}-{name}-{env}.{ext}`,
                ],
                params: {
                    module: this.module.name.name,
                    app: this.app.name,
                    name: this.container.name.name,
                },
                env: true,
            };
        },
    };

    public get app() {
        return this.container.app;
    }

    public get module() {
        return this.container.container;
    }

    public on() {
        return this.config.subject;
    }

    public async run() {
        if(this.config instanceof ConfigFile){
            await this.config.run();
            this.type = TypeConfig.File;
            return;
        }
        else if(this.config instanceof ConfigProperty){
            this.type = TypeConfig.Data;
            return;
        }
        try {
            let path = this._path(this.options.module);
            if (this.module.config.get(path, false)) {
                this.type = TypeConfig.Parent;
                this.config = this.module.config.use(path);
                log.info(`id: ${this.container.name.id} parent: module path: ${path} full: ${this.config.full()}`)
                return;
            }
        } catch (e) {
            log.error(`id: ${this.container.name.id}  parent: module  error: ${e.message}`)
        }
        try {
            let path = this._path(this.options.app);
            if (this.app.config.get(path, false)) {
                this.type = TypeConfig.Parent;
                this.config = this.app.config.use(path);
                log.info(`id: ${this.container.name.id} parent: app path: ${path} full: ${this.config.full()}`)
                return;
            }
        } catch (e) {
            log.error(`id: ${this.container.name.id}  parent: app  error: ${e.message}`)
        }
        const container = new ConfigFile<Data>(this._config(this.options.component));
        await container.run();
        this.type = TypeConfig.File;
        this.config = container;
    }

}
