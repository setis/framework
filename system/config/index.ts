import {ConfigModule} from "./module";
import {ConfigComponent} from "./component";
import {ConfigApp} from "./app";

export * from './module'
export * from './component'
export * from './app'

export type Config = ConfigModule|ConfigComponent|ConfigApp
