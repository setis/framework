import {expect} from "chai";
import {ConfigFile, ConfigProperty} from "../../../elements/configure";

describe('configure', function() {
    const data = require('./cfg.json');
    describe('file',()=>{
        const file = new ConfigFile({
            path:__dirname,
            pattern:[
                `root.{ext}`,
                `root-{env}.{ext}`
            ],
            timeout:1e3,
            env: true
        });
        beforeEach(async function(){
            await file.run();
        });
        it('загрузка run', async function () {
            expect(file.data).deep.equal(data)
        });
        it('loading',async function () {
            expect(file.loading()).eq(true);
            const time = file.time.getTime();
            await file.run();
            await file.run();
            expect(file.time.getTime()).eq(file.time.getTime());
            expect(file.time.getTime()-time).lte(file.timeout+100);

        })
    });
    describe('property',()=>{
        const config = new ConfigProperty() ;
        config.data = data;
        it('get',async function(){
            expect(config.get()).deep.equal(data);
            expect(config.get('config.b.config.c')).deep.equal(data.config.b.config.c);
            expect(config.get('config.b.config.c.id')).equal(data.config.b.config.c.id);
            expect(config.get('config.b.config.c.config',null)).eq(null);
            expect(()=>{
                config.get('name');
            }).to.throw(Error,'action: get path: name');

        });
        it('has',function(){
            expect(config.has('name')).eq(false)
            expect(config.has('id')).eq(true)
        });
        it('delete',function(){
            const config = new ConfigProperty() ;
            config.data = data;
            const path = 'config';
            config.delete(path);
            expect(config.get(path,null)).eq(null);
        });
        it('set',function(){
            const config = new ConfigProperty() ;
            config.data = data;
            const f = Math.random();
            const path = 'config.b.config.c.id';
            config.set(path,f);
            expect(config.get(path)).equal(f);
        });
        describe('subject',() => {
            it('update', function (done) {
                const config = new ConfigProperty() ;
                config.data = data;
                const f = Math.random();
                const path = 'config.b.config.c.id';
                const old = config.get(path)
                config.subject.subscribe(value => {
                    expect(value.link).eq(path);
                    expect(value.type).eq('update');
                    expect(value.value).eq(f);
                    expect(value.oldValue).eq(old);
                    done()
                });
                config.set(path,f);
                expect(config.get(path)).equal(f);
            });
            it('insert',function(done){
                const config = new ConfigProperty() ;
                config.data = data;
                const f = Math.random();
                const path = 'name';
                config.subject.subscribe(value => {
                    expect(value.link).eq(path);
                    expect(value.type).eq('insert');
                    expect(value.value).eq(f);
                    done()
                });
                config.set(path,f);
                expect(config.get(path)).equal(f);
            });
            it('delete',function(done){
                const config = new ConfigProperty() ;
                config.data = data;
                const path = 'config.b.config.c.id';
                const old = config.get(path)
                config.subject.subscribe(value => {
                    expect(value.link).eq(path);
                    expect(value.type).eq('delete');
                    expect(value.oldValue).eq(old);
                    done()
                });
                config.delete(path);
                expect(config.has(path)).equal(false);
            });
        });
        it('use', function(){
            expect(()=>{
                config.use('name')
            }).throw(Error,'нет такого name');
            const use = config.use('config.a');
            expect(use.get()).deep.equal(data.config.a);
            expect(use.get('id')).eq(data.config.a.id);
            expect(use.get('name',null)).eq(null);
            expect(config.use('config.a')).eq(use)
        });
        describe('use',() => {
            const config = new ConfigProperty() ;
            config.data = data;
            const path = 'config.a';
            const chunk = data.config.a;
            const use = config.use(path);
            it('full',() => {
                expect(use.full('id')).eq(`${path}.id`);
                expect(use.full()).eq(path);
            });
            it('get',() => {
                expect(use.get('id')).eq(chunk.id);
                expect(use.get('name',null)).eq(null);
                expect(()=>{
                    config.get('name');
                }).to.throw(Error,'action: get path: name');
            });
            it('has',function () {
                expect(config.has('name')).eq(false)
                expect(config.has('id')).eq(true)
            });
            it('delete',function(){
                const config = new ConfigProperty() ;
                config.data = data;
                const path1 = 'config.a';
                const chunk = data.config.a;
                const use = config.use(path1);
                use.delete(path);
                expect(use.get(path,null)).eq(null);
            });
            it('set',function(){
                const config = new ConfigProperty() ;
                config.data = data;
                const path1 = 'config.a';
                const chunk = data.config.a;
                const use = config.use(path1);
                const f = Math.random();
                const path = 'name';
                config.set(path,f);
                expect(config.get(path)).equal(f);
            });

        })
    });

});
