import {expect, use} from "chai";
import * as chaiAsPromised from 'chai-as-promised';
import * as sinonChai from 'sinon-chai';
import {RetryIntricate, RetrySimple} from "../../elements/retry";

use(chaiAsPromised);
use(sinonChai);

describe('retry',function(){
    describe('intricate',function(){
        const label = 'increment';
        it('success',async function(){
            let i = 0;
            const retry = new RetryIntricate({
                timeout:3,
                counter: 0,
                count: 5,
            });
            retry.store.set(label, async ()=> {
                i++;
                if(i === 1){
                    throw new Error(`${i}`)
                }
                return i;
            });
            return expect(retry.run(label)).eventually.equal(2)
        });
        it('fail',async function(){
            let i = 0;
            const retry = new RetryIntricate({
                timeout:3,
                counter: 0,
                count: 5,
            });
            retry.store.set(label, async ()=> {
                i++;
                throw new Error(`${i}`)
            })
            return expect(retry.run(label)).to.be.rejectedWith(Error,`6`);

        });

    });
    describe('simple',async function(){
        it('success',async function(){
            let i = 0;
            const retry = new RetrySimple({
                timeout:3,
                counter: 0,
                count: 5,
                call:async ()=>{
                    i++;
                    if(i === 1){
                        throw new Error(`${i}`)
                    }
                    return i;
                },
                check:() => {
                    return i > 1;
                }
            });
            return expect(retry.run()).eventually.eq(2)
        });
        it('fail',async function(){
            let i = 0;
            const retry = new RetrySimple({
                timeout:3,
                counter: 0,
                count: 5,
                call:async ()=>{
                    i++;
                    throw new Error(`${i}`)
                },
                check: ()=> i > 1000
            });
            return expect(retry.run()).to.be.rejectedWith(Error,`6`);

        });


    });
});
