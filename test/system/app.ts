import App, {ConfigApp} from "../../system";
import {EventEmitter} from "events";
import * as chai from "chai";
import {expect} from "chai";
import * as chaiAsPromised from 'chai-as-promised';
import {join} from "path";
import {ConfigProperty} from "../../elements/configure";
import {TypeConfig} from "../../system/config/base";

chai.use(chaiAsPromised);
describe('app',function(){
    const root = join(__dirname,'resource')
    const options = {
        notification:(new EventEmitter()).setMaxListeners(0),
        config:{
            path: `${root}/configure/`,
            pattern: [
                `{name}-{env}.{ext}`,
                `{name}.{env}.{ext}`,
                `{name}.{ext}`
            ],
            params: {
                name: 'app'
            },
            env: true
        },
        path: root
    };
    it('new', async function () {
        expect(()=>new App(options)).not.throw;
    });
    it('onInit/onDestroy', async function () {
        const app = new App(options);
        expect(app.onInit()).eventually.not.throw;

        expect(app.config).instanceOf(ConfigApp);
        expect(app.config.config).instanceOf(ConfigProperty);
        expect(app.config.type).eq(TypeConfig.File);
        expect(app.config.get()).deep.eq(require('./resource/configure/app.json'));



        expect(app.onDestroy()).eventually.not.throw;
    });

    it('start', async function () {
        this.timeout(11e3);
        const app = new App(options);
        await app.onInit();
        expect(Array.from(app.components()).length).eq(0);
        expect(Array.from(app.modules()).length).eq(0);
        await app.start();
        await new Promise(resolve => setTimeout(resolve,5e3));
        expect(Array.from(app.components()).length).eq(1);
        expect(Array.from(app.modules()).length).eq(1);
        expect(Array.from(app.modules()).map(value => value.name.id)).deep.eq(['component']);
        expect(Array.from(app.components()).map(value => value.name.id)).deep.eq(['component://interval']);
        await app.stop();
        await app.onDestroy();
    });
    // it('stop', async function () {
    //     const app = new App(options);
    //     expect(app.onInit()).eventually.ok;
    //     expect(app.start()).eventually.ok;
    //     this.timeout(10e3);
    //     await new Promise(resolve => setTimeout(resolve,5e3));
    //     expect(app.stop()).eventually.ok;
    // });
    // it('destroy', async function () {
    //     const app = new App(options);
    //     expect(app.onInit()).eventually.ok;
    //     expect(app.start()).eventually.ok;
    //     this.timeout(10e3);
    //     await new Promise(resolve => setTimeout(resolve,5e3));
    //     expect(app.stop()).eventually.ok;
    //     expect(app.onDestroy()).eventually.ok;
    // });
})
