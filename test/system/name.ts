import {expect} from 'chai'
import {Name, NameComponent, NameModule} from "../../system/name";
import {TypeLevelEnum} from "../../system";

describe('name', () => {
    describe('component', () => {
        describe('component', () => {
            const cfg1 = {
                module: 'component',
                name: 'http',
                version: '1'
            };
            const cfg2 = {
                module: 'component',
                component: 'http@1',
            };
            const id = 'component://http@1';
            it('new module,name,version ', function () {
                expect(() => {
                    new NameComponent(cfg1)
                }).to.not.throw;
            });
            it('new module,component ', function () {
                expect(() => {
                    new NameComponent(cfg2)
                }).to.not.throw;
            });
            it('check', () => {
                const name1 = new NameComponent(cfg1)
                expect(name1.id).eq(id);
                expect(name1.toString()).eq(id);
                expect(name1.module).eq('component');
                expect(name1.component).eq('http@1');
                expect(name1.name).eq('http');
                expect(name1.version).eq('1')

                const name2 = new NameComponent(cfg2);
                expect(name2.id).eq(id);
                expect(name2.toString()).eq(id);
                expect(name2.module).eq('component');
                expect(name2.component).eq('http@1');
                expect(name2.name).eq('http');
                expect(name2.version).eq('1')
            });
            it('change id', function () {
                const name = new NameComponent(cfg1);
                const id = 'middleware://cors@2';
                name.id = id;
                expect(name.id).eq(id);
                expect(name.toString()).eq(id);
                expect(name.module).eq('middleware');
                expect(name.component).eq('cors@2');
                expect(name.name).eq('cors');
                expect(name.version).eq('2')

            });
            it('change module', function () {
                const name = new NameComponent(cfg1);

                name.module = 'service';
                expect(name.module).eq('service');
                const id = 'service://http@1';

                expect(name.id).eq(id);
                expect(name.toString()).eq(id);

                expect(name.component).eq('http@1');
                expect(name.name).eq('http');
                expect(name.version).eq('1')

            });

            it('change component', function () {
                const name = new NameComponent(cfg1);
                name.component = 'typeorm@2';
                const id = 'component://typeorm@2';
                expect(name.id).eq(id);
                expect(name.toString()).eq(id);

                expect(name.component).eq('typeorm@2');
                expect(name.name).eq('typeorm');
                expect(name.version).eq('2')
            })
            it('change name', function () {
                const name = new NameComponent(cfg1);
                name.name = 'typeorm';
                const id = 'component://typeorm@1';
                expect(name.id).eq(id);
                expect(name.toString()).eq(id);

                expect(name.component).eq('typeorm@1');
                expect(name.name).eq('typeorm');
                expect(name.version).eq('1')
            })
            it('change version', function () {
                const name = new NameComponent(cfg1);
                name.version = '3';
                const id = 'component://http@3';
                expect(name.id).eq(id);
                expect(name.toString()).eq(id);

                expect(name.component).eq('http@3');
                expect(name.name).eq('http');
                expect(name.version).eq('3')
            })
        });
        describe('module', () => {
            const cfg = {
                name: 'component',
                version: '1'
            }
            const id = 'component@1'
            it('new', function () {
                expect(() => {
                    new NameModule(cfg)
                }).to.not.throw;
            });
            it('check', function () {
                const name = new NameModule(cfg);
                expect(name.id).eq(id);
                expect(name.toString()).eq(id);
                expect(name.name).eq(cfg.name);
                expect(name.version).eq(cfg.version);

            });
            it('change id', function () {
                const name = new NameModule(cfg);
                const id = 'middleware';
                name.id = id;
                expect(name.id).eq(id);
                expect(name.name).eq('middleware');
                expect(name.version).eq(undefined);
                expect(name.id).eq('middleware')

            });
            it('change name', function () {
                const name = new NameModule(cfg);
                expect(name.id).eq(id);
                name.name = 'middleware';
                expect(name.name).eq('middleware');
                expect(name.version).eq('1');
                expect(name.id).eq('middleware@1')
            });
            it('change version', function () {
                const name = new NameModule(cfg);
                expect(name.id).eq(id);
                name.version = '3';
                expect(name.name).eq('component');
                expect(name.version).eq('3');
                expect(name.id).eq('component@3')
            });

        });
    });
    describe('module', () => {
        it('new ', function () {
            expect(() => new Name()).not.throw;
        });
        it('factory', function () {
            expect(Name.factory('component://http')).instanceOf(NameComponent);
            expect(Name.factory({
                module:'component',
                component: 'http',
                type: TypeLevelEnum.Component
            })).instanceOf(NameComponent);
            expect(Name.factory('component')).instanceOf(NameModule);
            expect(Name.factory({
                name: 'component',
                type: TypeLevelEnum.Module
            })).instanceOf(NameModule);
            expect(()=>Name.factory([1,2] as any)).throw(Error)
        });
        it('is', function () {
            expect(Name.is(new NameComponent('component://http'))).eq(true);
            expect(Name.is(new NameModule('component'))).eq(true);
            expect(Name.is(new Object())).eq(false);
        });
        it('use', function () {
            const name = new Name();
            const component = name.use('component://http') as NameComponent;
            expect(component).instanceOf(NameComponent);
            expect(name.store.size).eq(1);
            expect(name.use({
                component: component.component,
                module: component.module,
                type: component.type
            })).eq(component)
            expect(name.store.size).eq(1);
            const module = name.use('component');
            expect(name.store.size).eq(2);
            expect(module).instanceOf(NameModule);
            expect(name.use({
                name: module.name,
                version: module.version,
                type: module.type
            })).eq(module);
            expect(name.store.size).eq(2);
            expect(Array.from(name.store.keys())).deep.equal([component.id,module.id])
            expect(()=>name.use([1,2] as any)).throw(Error)
        });
        it('add ', function () {
            const name = new Name();
            expect(name.store.size).eq(0);
            const id = 'component://http';
            name.add(new NameComponent(id));
            expect(()=> name.add(new Object() as any)).throw(Error);
            expect(name.store.size).eq(1);
            expect(Array.from(name.store.keys())).deep.equal([id])
        });
        it('remove', function () {
            const name = new Name();
            const id = 'component://http';
            const l1 = new NameComponent(id);
            name.add(l1);
            name.remove(l1);
            expect(name.store.size).eq(0);
            name.add(l1);
            name.remove(id);
            expect(name.store.size).eq(0);
            name.add(l1);
            name.remove({
                type:TypeLevelEnum.Component,
                module:'component',
                component:'http'
            });
            expect(name.store.size).eq(0);
            expect(()=>name.remove(new Object()as any)).throw(Error)
        });
    })

});
