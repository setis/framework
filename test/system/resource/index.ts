import {EventEmitter} from "events";
import App from "../../../system";

const options = {
    notification:(new EventEmitter()).setMaxListeners(0),
    config:{
        path: `${__dirname}/configure/`,
        pattern: [
            `{name}-{env}.{ext}`,
            `{name}.{env}.{ext}`,
            `{name}.{ext}`
        ],
        params: {
            name: 'app'
        },
        env: true
    },
    path: __dirname
};
const app = new App(options);
(async ()=>{
    await app.onInit();
    await app.start();
    await new Promise(resolve => setTimeout(resolve,1e3));
    console.log('stop!!!!!!!!!!!!!!!!!!!!')
    await app.stop();
    await app.onDestroy();
})();
