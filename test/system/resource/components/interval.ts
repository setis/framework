import {Component, ComponentComponent} from "../../../../system";
import Timeout = NodeJS.Timeout;

export const name = 'interval';
export default class Interval implements Component{

    constructor(readonly api: ComponentComponent){

    }
    protected id:Timeout;
    start() {
        if(!this.id){
            this.id = setInterval(this.handler.bind(this),this.api.config.get('timeout'))
        }
    }
    protected handler(){
        console.log(new Date())
    }
    stop() {
        if(this.id){
            clearInterval(this.id);
            delete this.id;
        }
    }

}
